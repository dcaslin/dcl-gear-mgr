import { DclGearMgrPage } from './app.po';

describe('dcl-gear-mgr App', function() {
  let page: DclGearMgrPage;

  beforeEach(() => {
    page = new DclGearMgrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
