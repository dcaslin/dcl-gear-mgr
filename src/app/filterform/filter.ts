/**
 * Created by davecaslin on 1/17/17.
 */

import {DamageType, ClassAllowed} from "../services/model/item";
import {stat} from "fs";


export class ParsedFilter{

  searchText: string;
  tags: string[];
  type: string[];

  rarity: string[];
  owner: string[];

  leveled: boolean;
  equipped: boolean;

  minLight: number;
  maxLight: number;
  dmgType: DamageType;

  minQual: number;
  maxQual: number;
  statSplit: string;
  classAllowed: string[];
  onlyShowCompare: boolean;

  constructor(searchText: string, tags: string[], type: string[], rarity: string[], owner: string[],
              leveled: boolean, equipped: boolean, minLight: number, maxLight: number, dmgType: DamageType,
              minQual: number, maxQual: number, statSplit: string, classAllowed: string[],
              onlyShowCompare: boolean){
    this.onlyShowCompare = onlyShowCompare;
    this.searchText = searchText;
    this.tags = tags;
    this.type = type;

    this.rarity = rarity;
    this.owner = owner;

    this.leveled = leveled;
    this.equipped = equipped;

    this.minLight = minLight;
    this.maxLight = maxLight;
    this.dmgType = dmgType;

    this.minQual = minQual;
    this.maxQual = maxQual;
    this.statSplit = statSplit;
    this.classAllowed = classAllowed;
  }

}

export class ItemFilter{
  leveled: string;
  equipped: string;
  rarity: Choice[];
  owner: Choice[];
  tags: Choice[];
  type: Choice[];

  minLight: number;
  maxLight: number;
  searchText: string;
  dmgType: DamageType;
  ornament: boolean;

  minQual: number;
  maxQual: number;
  statSplit: string;
  classAllowed: Choice[];

  onlyShowCompare: boolean = false;


  parse(): ParsedFilter{

    if (this.dmgType as any =="null"){
      this.dmgType = null;
    }
    if (this.classAllowed as any =="null"){
      this.classAllowed = null;
    }
    if (this.statSplit as any =="null"){
      this.statSplit = null;
    }

    let p: ParsedFilter = new ParsedFilter(this.searchText,
      ItemFilter.parseChoices(this.tags), ItemFilter.parseChoices(this.type),
      ItemFilter.parseChoices(this.rarity), ItemFilter.parseChoices(this.owner),
      ItemFilter.parseBoolean(this.leveled),
      ItemFilter.parseBoolean(this.equipped),
      this.minLight, this.maxLight, this.dmgType,
      this.minQual, this.maxQual, this.statSplit, ItemFilter.parseChoices(this.classAllowed),
      this.onlyShowCompare);
    return p;
  }

  static parseBoolean(str: string){
    if (str==null || str.length==0 || str=="null") return null;
    if (str=="true") return true;
    return false;
  }

  static parseChoices(ch: Choice[]): string[]{
    if (ch==null) return null;
    let r: string[] = [];
    ch.forEach(
      x => {
        if (x.children==null || x.children.length==0) {
          if (x.getValue() === true) {
            r.push(x.field);
            if (x.otherFields!=null){
              Array.prototype.push.apply(r, x.otherFields);
            }
          }
        }
        else{
          x.children.forEach(
            y => {
              if (y.getValue() === true){
                r.push(y.field);
              }
            }
          );
        }
      }
    )
    return r;
  }
}

export class Choice{
  readonly field: string;
  readonly otherFields: string[];
  readonly display: string;
  private _value: boolean = true;
  parent: Choice;
  children: Choice[] = [];

  constructor(field: string, display: string, value?: boolean, children?:Choice[], otherFields?:string[]){
    this.field = field;
    this.display = display;
    if (value) this._value = value;
    if (children) this.children = children;
    this.children.forEach(
      ch => ch.parent = this
    )
    this.otherFields = otherFields;
  }

  getValue(): boolean{
    return this._value;
  }

  setValue(value: boolean) {
    this._value = value;
    this.children.forEach(
      ch => ch._value = value
    );
    if (!value && this.parent){
      this.parent._value = false;
    }
    else if (value && this.parent){
      let allTrue: boolean = true;
      this.parent.children.forEach(ch => {if (!ch._value) allTrue = false;})
      if (allTrue){
        this.parent._value = true;
      }
    }
  }

}
