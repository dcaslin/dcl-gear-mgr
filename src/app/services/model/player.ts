/**
 * Created by davecaslin on 12/21/16.
 */

import {Injectable} from '@angular/core';


export enum Platform {
  XBL = 1,
  PSN = 2
}

export class PlayerId{
  public platform: Platform;
  public displayName: string;
  public id: string;

  constructor(platform: Platform, displayName: string, id: string){
    this.platform = platform;
    this.displayName = displayName;
    this.id = id;
  }

}

export class Player {

  public id: PlayerId;
  public otherPlatform: PlayerId;
  public glimmer: number;
  public marks: number;

  public getOtherPlatformName(){
    if (this.otherPlatform==null) return "";
    return Platform[this.otherPlatform.platform];
  }

  public getPlatformName(){
    return Platform[this.id.platform];
  }

  constructor(id: PlayerId, otherPlatform: PlayerId) {
    this.id = id;
    this.otherPlatform = otherPlatform;
  }
}
