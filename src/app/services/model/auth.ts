/**
 * Created by Dave on 12/20/2016.
 */

import {AccessToken} from './access-token';

export class Auth {
  accessToken: AccessToken;
  refreshToken: AccessToken;
  scope: number;

  constructor(obj: any) {

    this.scope = obj.scope;
    this.accessToken = new AccessToken(obj.accessToken);
    this.refreshToken = new AccessToken(obj.refreshToken);
  }

}
