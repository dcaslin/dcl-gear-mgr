import {Progression} from "./progression";
import {Target} from "./character";
/**
 * Created by davecaslin on 12/21/16.
 */

export enum DamageType {
  Unknown = 0,
  Kinetic = 1,
  Arc = 2,
  Solar = 3,
  Void = 4
}

export enum ClassAllowed {
  Titan = 0,
  Hunter = 1,
  Warlock = 2,
  Any = 3
}

export class Cost {
  readonly name: string;
  readonly value: number;

  constructor(name: string, value: number){
   this.name = name;
   this.value = value;
  }
}

export class Quality {
  readonly qualityPct: number;
  readonly intPct: number;
  readonly strPct: number;
  readonly disPct: number;
  readonly statSplit: string; //enum?  ID, IS, DS


  constructor(qualityPct, intPct, disPct, strPct, statSplit) {
    this.qualityPct = qualityPct;
    this.intPct = intPct;
    this.disPct = disPct;
    this.strPct = strPct;
    this.statSplit = statSplit;
  }
}


export abstract class Item {
  readonly name: string;
  readonly type: string;
  readonly light: number;
  readonly icon: string;
  readonly bucketName: string; //enum?
  readonly itemType: string; //enum?
  readonly tierTypeName: string; //enum?
  readonly classAllowed: ClassAllowed;
  readonly classText: string;
  readonly steps: Progression;
  id: string;
  readonly hash: number;
  readonly dmgType: DamageType;
  readonly pctComplete: number;
  readonly currentXP: number;
  readonly totalXP: number;
  readonly xpNeeded: number;
  readonly desc: string;

  postmaster: boolean = false;
  mark: string = null;
  markClass: string = null;
  markLabel: string = null;
  notes: string = "";
  fav: boolean = false;

  locked: boolean;
  owner: Target;
  options: Target[];

  equipped: boolean = false;

  compare: boolean = false;
  vendor: string = null;
  costs: Cost[] = null;

  constructor(name: string, type: string, light: number, icon: string, bucketName: string, itemType: string,
              tierTypeName: string, classAllowed: ClassAllowed, steps: Progression, locked: boolean, id: string,
              hash: number, equipped: boolean, dmgType: DamageType, desc?: string) {
    this.name = name;
    this.type = type;
    this.light = light;
    this.icon = icon;
    this.bucketName = bucketName;
    this.itemType = itemType;
    this.tierTypeName = tierTypeName;
    this.classAllowed = classAllowed;
    this.steps = steps;
    this.classText = ClassAllowed[this.classAllowed];
    this.locked = locked;
    this.id = id;
    this.hash = hash;
    this.equipped = equipped;
    this.dmgType = dmgType;
    if (this.steps != null) {
      this.pctComplete = this.steps.pctComplete();
      this.currentXP = this.steps.currentXP;
      this.totalXP = this.steps.totalXP;
      this.xpNeeded = this.steps.xpNeeded();
    }
    else {
      this.pctComplete = 100;
      this.currentXP = 1;
      this.totalXP = 1;
      this.xpNeeded = 0;
    }
    this.desc = desc;
  }
}

export class Objective{
  readonly desc: string;
  readonly current: number;
  readonly total: number;
  readonly isCountingDownward: boolean;
  readonly pct: number;

  constructor(desc: string, current: number, total: number, isCountingDownward: boolean){
    this.desc = desc;
    this.current = current;
    this.total = total;
    this.isCountingDownward = isCountingDownward;
    let wTotal:number = this.total;
    if (this.total<1) wTotal = 1;
    this.pct = Math.floor(100 * this.current/this.total);
  }
}

export class Bounty extends Item {
  readonly objs: Objective[];
  readonly forSale: boolean;
  readonly pct: number;

  constructor(hash: number, forSale: boolean, name: string, type: string, icon: string, desc: string, objs: Objective[]) {
    super(name, type, 0, icon, null, null, null,
      null, null, null, null, hash, null, null, desc);
    this.forSale = forSale;
    this.objs = objs;

    if (!forSale) {
      let sum: number = 0;
      if (this.objs) {
        this.objs.forEach(function (obj) {
          sum += obj.pct;
        });
        sum = Math.floor(sum / this.objs.length);
      }
      this.pct = sum;
    }
    else{
      this.pct = 101;
    }


  }
}




export class Armor extends Item {
  readonly int: number;
  readonly dis: number;
  readonly str: number;

  readonly qualityPct: number;
  readonly intPct: number;
  readonly disPct: number;
  readonly strPct: number;
  readonly statSplit: string;


  constructor(name: string, type: string, light: number, icon: string, bucketName: string,
              itemType: string, tierTypeName: string, classAllowed: ClassAllowed, steps: Progression,
              quality: Quality, int: number, dis: number, str: number, locked: boolean, id: string, hash: number,
              equipped: boolean, damageType: DamageType, desc: string) {
    super(name, type, light, icon, bucketName, itemType, tierTypeName,
      classAllowed, steps, locked, id, hash, equipped, damageType, desc);
    this.int = int;
    this.dis = dis;
    this.str = str;
    if (quality != null) {
      this.qualityPct = quality.qualityPct;
      this.intPct = quality.intPct;
      this.disPct = quality.disPct;
      this.strPct = quality.strPct;
      this.statSplit = quality.statSplit;
    }
    else {
      this.qualityPct = -1;
      this.intPct = -1;
      this.disPct = -1;
      this.strPct = -1;
      this.statSplit = "";
    }
  }
}

export class Subclass extends Item {
  constructor(name: string, type: string, light: number, icon: string, bucketName: string,
              itemType: string, tierTypeName: string, classAllowed: ClassAllowed,
              steps: Progression, locked: boolean, id: string, hash: number,
              equipped: boolean) {
    super(name, type, light, icon, bucketName, itemType, tierTypeName, classAllowed, steps, locked, id, hash, equipped, null);
  }
}

export class Weapon extends Item {
  readonly impact: number;
  readonly range: number;
  readonly stability: number;
  readonly rof: number;
  readonly reload: number;
  readonly magazine: number;
  readonly velocity: number;
  readonly blastRadius: number;
  readonly chargeRate: number;
  readonly speed: number;
  readonly defense: number;
  readonly energy: number;
  readonly efficiency: number;
  readonly aa: number;
  readonly inv: number;
  readonly optics: number;
  readonly recoil: number;
  readonly blessedPve: number;
  readonly blessedPvp: number;
  readonly roll: number;

  private static getMatchGrade(matchAmount): string {
    switch (matchAmount) {
      case 0:
        return "";
      case 1:
        return "";
      case 2:
        return "Ok";
      case 3:
        return "Good";
      case 4:
        return "Great";
      default:
        return "";
    }
  }


  private static getShortGrade(matchAmount): string {
    switch (matchAmount) {
      case 0:
        return "";
      case 1:
        return "";
      case 2:
        return "C";
      case 3:
        return "B";
      case 4:
        return "A";
      default:
        return "";
    }
  }

  pveRollRank(){
    return Weapon.getMatchGrade(this.blessedPve);
  }

  pvpRollRank(){
    return Weapon.getMatchGrade(this.blessedPvp);
  }


  abbrevPveRollRank(){
    let rank:string = Weapon.getShortGrade(this.blessedPve);
    if (rank===null) return "";
    if (rank==="") return "";
    return Weapon.getShortGrade(this.blessedPve)+" pve";
  }

  abbrevPvpRollRank(){
    let rank:string = Weapon.getShortGrade(this.blessedPvp);
    if (rank===null) return "";
    if (rank==="") return "";
    return Weapon.getShortGrade(this.blessedPvp)+" pvp";
  }


  constructor(name: string, type: string, light: number, icon: string, bucketName: string,
              itemType: string, tierTypeName: string, classAllowed: ClassAllowed, steps: Progression,
              impact: number, range: number, stability: number, rof: number, reload: number, magazine: number,
              velocity: number, blastRadius: number, chargeRate: number, speed: number, defense: number, energy: number, efficiency: number,
              aa: number, inv: number, optics: number, recoil: number,
              dmgType: DamageType, locked: boolean, id: string, hash: number,
              equipped: boolean, blessedPve: number, blessedPvp: number, desc: string) {
    super(name, type, light, icon, bucketName, itemType, tierTypeName, classAllowed,
      steps, locked, id, hash, equipped, dmgType, desc);
    this.impact = impact;
    this.range = range;
    this.stability = stability;
    this.rof = rof;
    this.reload = reload;
    this.magazine = magazine;
    this.velocity = velocity;
    this.blastRadius = blastRadius;
    this.chargeRate = chargeRate;
    this.speed = speed;
    this.defense = defense;
    this.energy = energy;
    this.efficiency = efficiency;
    this.aa = aa;
    this.inv = inv;
    this.optics = optics;
    this.recoil = recoil;
    this.blessedPve = blessedPve;
    this.blessedPvp = blessedPvp;
    this.roll = blessedPve+blessedPvp;

  }
}


export class Engram extends Item {

  constructor(name: string, type: string, light: number, icon: string, bucketName: string,
              itemType: string, tierTypeName: string, classAllowed: ClassAllowed, steps: Progression,
              locked: boolean, id: string, hash: number) {
    super(name, type, light, icon, bucketName, itemType, tierTypeName, classAllowed, steps, locked, id, hash, false, null);
  }
}

