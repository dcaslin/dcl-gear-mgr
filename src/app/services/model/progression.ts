import {DamageType} from "./item";
/**
 * Created by davecaslin on 12/21/16.
 */


export class Step {
  readonly col: number;
  readonly name: string;
  readonly desc: string;
  readonly activated: boolean;
  readonly hidden: boolean;
  blessedPve: boolean;
  blessedPvp: boolean;

  constructor(col, name, desc, activated, hidden) {
    this.col = col;
    this.name = name;
    this.desc = desc;
    this.activated = activated;
    this.hidden = hidden;
  }
}

export class StepGroup {
  public readonly steps: Step[];

  constructor(steps: Step[]) {
    this.steps = steps;
  }
}

export class Progression {
  readonly cols: StepGroup[];
  readonly hasI: boolean;
  readonly hasD: boolean;
  readonly hasS: boolean;

  readonly activeIntBonus: boolean;
  readonly activeDisBonus: boolean;
  readonly activeStrBonus: boolean;

  readonly currentXP: number;
  readonly totalXP: number;
  readonly armorDmgType: DamageType;
  readonly searchText: string;
  readonly perkNames: string[];
  readonly hasOrnament: boolean;

  isComplete(): boolean {
    return this.currentXP >= this.totalXP;
  }

  pctComplete(): number {
    if (this.totalXP == 0) return 100;
    let pct: number = Math.floor(100 * (this.currentXP / this.totalXP));
    return Math.min(pct, 100);
  }

  xpNeeded(): number {
    return this.totalXP - this.currentXP;
  }

  constructor(cols:StepGroup[], hasI: boolean, hasD: boolean, hasS: boolean, activeIntBonus: boolean,
              activeDisBonus: boolean, activeStrBonus: boolean, currentXP: number, totalXP: number,
              armorDmgType: DamageType, hasOrnament: boolean) {
    this.cols = cols;
    let searchText: string = "";
    let perkNames: string[] = [];
    this.cols.forEach(
      sg =>{
        sg.steps.forEach(
          y=>
          {
            searchText+=y.name+" ";
            perkNames.push(y.name);
          }
        )
      }
    );
    this.hasOrnament = hasOrnament;
    if (this.hasOrnament==true){
      searchText+="ornament"+" ";
    }
    this.searchText = searchText.toLowerCase();
    this.hasI = hasI;
    this.hasD = hasD;
    this.hasS = hasS;
    this.activeIntBonus = activeIntBonus;
    this.activeDisBonus = activeDisBonus;
    this.activeStrBonus = activeStrBonus;
    this.currentXP = currentXP;
    this.totalXP = totalXP;
    this.armorDmgType = armorDmgType;
    this.perkNames = perkNames.sort();
  }
}
