/**
 * Created by Dave on 12/20/2016.
 */

export class AccessToken {
  public expires: number;
  public readyin: number;
  public value: string;
  public inception: Date;

  constructor(obj: any) {
    this.expires = obj.expires;
    if (obj.readyin)
      this.readyin = obj.readyin;
    else
      this.readyin = null;
    this.value = obj.value;
    this.inception = new Date(obj.inception as string);
  }

  public isValid(): boolean {
    let p: Date = new Date(this.inception.valueOf() + this.expires * 1000 - 1800000);
    let practicalExp: number = p.valueOf();
    let now: number = (new Date()).valueOf();
    //if it's expired it's not good
    if (practicalExp <= now) return false;
    if (this.readyin){
      let ready: number = new Date(this.inception.valueOf() + this.readyin * 1000).valueOf();
      if (ready >= now) return false;
    }
    return true;
  }
}
