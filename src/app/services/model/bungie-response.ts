/**
 * Created by Dave on 12/20/2016.
 */
import {Injectable} from '@angular/core';
import {AuthService} from "../auth.service";

@Injectable()
export class BungieResponse {
  ErrorCode: number;
  ErrorStatus: string;
  Message: string;
  ThrottleSeconds: number;
  Response: any;

  constructor(obj: any) {
    this.ErrorCode = obj.ErrorCode;
    this.ErrorStatus = obj.ErrorStatus;
    this.Message = obj.Message;
    this.ThrottleSeconds = obj.ThrottleSeconds;
    this.Response = obj.Response;
    if (this.ErrorCode != 1 ){
      if (this.ErrorStatus=="WebAuthRequired"){
        AuthService.reroute();
        return;
      }
      else if (this.ErrorCode==2106){
        AuthService.reroute();
        return;
      }
      if (this.Message!=null) throw new Error(this.Message);
      else throw new Error("Unknown error");
    };
  }
}
