/**
 * Created by Dave on 12/21/2016.
 */
import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, ResponseContentType, RequestMethod} from "@angular/http";
import {AuthService} from "./auth.service";
import {NotificationService} from "../services/notification.service";
import {DestinyCacheService} from "../services/destiny-cache.service";
import {BungieResponse} from "./model/bungie-response";
import {Player, PlayerId, Platform} from "./model/player";
import {Character} from "./model/character";
import {Item} from "./model/item";
import {environment} from "../../environments/environment";
import "rxjs/add/operator/toPromise";

@Injectable()
export class BungieService {

  constructor(private http: Http, private authService: AuthService, private notificationService: NotificationService,
              private destinyCacheService: DestinyCacheService) {
  }

  private post(url: string, payload: any): Promise<BungieResponse> {
    this.notificationService.thinking(true);
    let self: BungieService = this;
    return this.authService.getKey().then(function (key) {
      let opts: RequestOptions = new RequestOptions(
        {
          method: RequestMethod.Post,
          withCredentials: true,
          responseType: ResponseContentType.Json,
          headers: new Headers({
            'X-API-Key': environment.apiKey,
            'Authorization': 'Bearer ' + key
          })
        });
      return self.http.post(url, payload, opts)
        .map(
          function (res) {
            self.notificationService.thinking(false);
            let obj: Object = res.json();
            let bRes: BungieResponse = new BungieResponse(obj);
            return bRes;
          }).toPromise();
    });
  }


  private request(url: string): Promise<BungieResponse> {
    this.notificationService.thinking(true);
    let self: BungieService = this;
    let prom: Promise<string> = this.authService.getKey();

    return prom.then(function (key) {
      let opts: RequestOptions = new RequestOptions(
        {
          method: RequestMethod.Get,
          withCredentials: true,
          responseType: ResponseContentType.Json,
          headers: new Headers({
            'X-API-Key': environment.apiKey,
            'Authorization': 'Bearer ' + key
          })
        });

      return self.http.get(url, opts)
        .map(
          function (res) {
            self.notificationService.thinking(false);
            let obj: Object = res.json();
            let bRes: BungieResponse = new BungieResponse(obj);
            return bRes;
          }).toPromise();
    });
  }

  public getInventory(player: Player, char: Character): Promise<any> {
    return this.request("https://www.bungie.net/D1/Platform/Destiny/" + player.id.platform + "/Account/" + player.id.id +
      "/Character/" + char.id + "/Inventory/?definitions=false").then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public getVendor(player: Player, char: Character, vendorId: string): Promise<any>{
    return this.request(
      "https://www.bungie.net/D1/Platform/Destiny/"+player.id.platform+"/MyAccount/Character/"+char.id+"/Vendor/"+vendorId+"/").then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public setLockState(player: Player, char: Character, item: Item, locked: boolean): Promise<any> {
    return this.post("https://www.bungie.net/D1/Platform/Destiny/SetLockState/",

      {
        characterId: char.id,
        membershipType: player.id.platform,
        itemId: item.id,
        state: locked
      }
    ).then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public equip(player: Player, char: Character, item: Item): Promise<any> {
    return this.post("https://www.bungie.net/D1/Platform/Destiny/EquipItem/",
      {
        characterId: char.id,
        itemId: item.id,
        membershipType: player.id.platform,
      }
    ).then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public transfer(player: Player, char: Character, item: Item, toVault: boolean): Promise<any> {
    return this.post("https://www.bungie.net/D1/Platform/Destiny/TransferItem/",

      {
        characterId: char.id,
        itemId: item.id,
        itemReferenceHash: item.hash,
        membershipType: player.id.platform,
        stackSize: 1,
        transferToVault: toVault
      }
    ).then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public getVault(player: Player): Promise<any> {
    return this.request("https://www.bungie.net/D1/Platform/Destiny/" + player.id.platform + "/MyAccount/Vault/?definitions=false").then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        return data;
      }
    );
  }

  public getAccount(player: Player): Promise<Character[]> {
    let self: BungieService = this;

    return this.request("https://www.bungie.net/D1/Platform/Destiny/" + player.id.platform + "/Account/" + player.id.id + "/").then(
      function (bRes: BungieResponse) {
        let data: any = bRes.Response.data;
        data.inventory.currencies.forEach(function (curr: any) {
          if (curr.itemHash === 2534352370) {
            player.marks = curr.value;
          } else if (curr.itemHash === 3159615086) {
            player.glimmer = curr.value;
          }
        });

        let chars: Character[] = [];
        data.characters.forEach(function (bChar: any) {
          let char: Character = new Character(bChar, self.destinyCacheService.cache);
          chars.push(char);
        });
        return chars;
      }
    );
  }

  public setPreferredPlatform(platform: Platform) {
    localStorage.setItem("preferredPlatform", platform.toString());
    window.location.reload();
  }

  private static parsePlayerId(userInfo: any): PlayerId {
    let membershipId: string = userInfo.membershipId;
    let membershipType: number = userInfo.membershipType;
    let plat: Platform;
    if (membershipType == 1) {
      plat = Platform.XBL;
    }
    else if (membershipType == 2) {
      plat = Platform.PSN;
    }
    else {
      //ignore battle.net
      return null;
    }
    let displayName: string = userInfo.displayName;
    let id: PlayerId = new PlayerId(plat, displayName, membershipId);
    return id;
  }

  public getBungieNetUser(): Promise<Player> {
    return this.request("https://www.bungie.net/Platform/User/GetCurrentBungieAccount/").then(
      function (bRes: BungieResponse) {

        let data: any = bRes.Response;
        if (!data.destinyMemberships || data.destinyMemberships.length < 1) {
          throw new Error("No destiny accounts found!");
        }
        
        let playerIds: PlayerId[] = [];

        let playerId1: PlayerId = null;
        let playerId2: PlayerId = null;
        
        data.destinyMemberships.forEach(function(membership){
          let tempId: PlayerId = BungieService.parsePlayerId(membership);
          if (tempId!=null)
            playerIds.push(tempId);
        });
        if (playerIds.length==2){
          playerId1 = playerIds[0];
          playerId2 = playerIds[1];
        }
        else if (playerIds.length==1){
          playerId1 = playerIds[0];
        }
        else if (playerIds.length==0){
          throw new Error("No players found");
        }
        else{
          console.dir(playerIds);
          throw new Error("Unexpected player ID length: "+playerIds.length);
        }
        
        
        if (playerId2!=null) {
          
          if (localStorage.getItem("preferredPlatform") === "1") {
            if (playerId1.platform==Platform.XBL){
              return new Player(playerId1, playerId2);
            }
            else{
              return new Player(playerId2, playerId1);
            }
          }
          else{
            if (playerId1.platform==Platform.PSN){
              return new Player(playerId1, playerId2);
            }
            else{
              return new Player(playerId2, playerId1);
            }
          }
        }
        else{
          return new Player(playerId1, null);
        }
      }
    );

  }
}
