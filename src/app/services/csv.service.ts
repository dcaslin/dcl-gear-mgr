/**
 * Created by Dave on 1/5/2017.
 */
import {Injectable} from "@angular/core";
import {GearService} from "./gear.service";
import {Step, StepGroup} from "./model/progression";
import {Weapon, DamageType, ClassAllowed, Armor} from "./model/item";

@Injectable()
export class CsvService {
  
  constructor(private gearService: GearService) {

  }
  
  downloadWeaponsCsv(){
    let header:string ="Name, Tier, Type, Tag, Light, Dmg, Owner, Leveled, Locked, Equipped, " +
      "AA, Impact, Range, Stability, ROF, Reload, Mag, Notes,Perks\n";
    let data: string = "";    
    this.gearService.weapons.forEach(
      (g: Weapon)=>{
        data += g.name + ", ";
        data += g.tierTypeName + ", ";
        data += g.type + ", ";
        data += g.mark + ", ";
        data += g.light + ", ";
        data += DamageType[g.dmgType] + ", ";
        data += g.owner.label + ", ";
        data += !g.steps.xpNeeded() + ", ";
        data += g.locked + ", ";
        data += g.equipped + ", ";
        data += g.aa + ", ";
        data += g.impact + ", ";
        data += g.range + ", ";
        data += g.stability + ", ";
        data += g.rof + ", ";
        data += g.reload + ", ";
        data += g.magazine + ", ";
        if (g.notes){
          data += g.notes+",";
        }
        else{
          data += ",";
        }
        //add perks
        g.steps.cols.forEach(
          (col: StepGroup)=>
          {
            col.steps.forEach(
              (step: Step) => {
                data+=step.name;
                if (step.activated){
                  data+="*";
                }
                data+=",";
              }
            );
          }
        );
        data+="\n";
      }
    );

    this.downloadCsv("destinyWeapons", header+data);
  }

  
  downloadArmorCsv(){
    let header:string = "Name, Tier, Type, Tag, Class Allowed, Light, Owner, Leveled, Locked, Equipped, " +
      "Split, Quality, IntQ, DiscQ, StrQ, Int, Disc, Str, Notes, Perks\n";
    let data: string = "";
    this.gearService.armor.forEach(
      (a: Armor)=>{
        data += a.name + ", ";
        data += a.tierTypeName + ", ";
        data += a.type + ", ";
        data += a.mark + ", ";
        data += ClassAllowed[a.classAllowed] + ", ";
        data += a.light + ", ";
        data += a.owner.label + ", ";
        data += !a.steps.xpNeeded() + ", ";
        data += a.locked + ", ";
        data += a.equipped + ", ";
        data += a.statSplit + ", ";
        data += a.qualityPct + ", ";
        data += a.intPct + ", ";
        data += a.disPct + ", ";
        data += a.strPct + ", ";
        data += a.int + ", ";
        data += a.dis + ", ";
        data += a.str + ", ";
        if (a.notes){
          data += a.notes+",";
        }
        else{
          data += ",";
        }
        //add perks
        a.steps.cols.forEach(
          (col: StepGroup)=>
          {
            col.steps.forEach(
              (step: Step) => {
                data+=step.name;
                if (step.activated){
                  data+="*";
                }
                data+=",";
              }
            );
          }
        );
        data+="\n";
      }
    );

    this.downloadCsv("destinyArmor", header+data);
  }
  
  private downloadCsv(filename: string, csv: string){
    filename = filename+".csv";
    let anch: HTMLAnchorElement = document.createElement('a');
    anch.setAttribute("href", 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv));
    anch.setAttribute("download", filename);
    anch.click();
  }

}
