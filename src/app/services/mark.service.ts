/**
 * Created by Dave on 1/5/2017.
 */
import {Injectable} from "@angular/core";
import {Http, RequestOptions, ResponseContentType, RequestMethod} from "@angular/http";
import {NotificationService} from "./notification.service";
import {Item} from "./model/item";
import "rxjs/add/operator/toPromise";
import {environment} from "../../environments/environment";
import {Subject} from "rxjs";

declare let LZString: any;

export class Todo {
  text: string;
  focusMe: boolean;

  constructor(focusMe?: boolean) {
    if (!focusMe)
      this.focusMe = false;
    else {
      this.focusMe = focusMe;
    }
    this.text = "";
  }
}

export class Marks {
  marked: {[key: string]: string};
  notes: {[key: string]: string};
  favs: {[key: string]: boolean};
  todo: {[key: string]: Todo[]};
  platform: number;
  memberId: string;

  constructor(platform: number, memberId: string, marked: any, notes: any, favs: any, todo: any) {
    this.platform = platform;
    this.memberId = memberId;
    if (marked == null) this.marked = {};
    else this.marked = marked;
    if (notes == null) this.notes = {};
    else this.notes = notes;
    if (favs == null) this.favs = {};
    else this.favs = favs;
    if (todo == null) this.todo = {};
    else this.todo = todo;
  }
}

@Injectable()
export class MarkService {
  private currentMarks: Marks;
  public dirty: boolean = false;
  private marksChanged: Subject<boolean> = new Subject<boolean>();
  public dirtyFeed: Subject<void> = new Subject<void>();
  public favsUpdatedFeed: Subject<void> = new Subject<void>();

  constructor(private http: Http, private notificationService: NotificationService) {

    //auto-save every 5 seconds if dirty
    this.marksChanged.debounceTime(5000).subscribe(
      chg => {
        if (this.dirty === true) {
          this.saveMarks();
        }
      });
  }

  todoItems(charId: string): Todo[]{
        if (!(this.currentMarks && this.currentMarks.todo)) return [];
      if (!this.currentMarks.todo[charId]) return [];
      return this.currentMarks.todo[charId];
    }

  updateTodo(charId: string, todo: Todo[]){
      this.currentMarks.todo[charId] = todo;
      this.dirty = true;
      this.dirtyFeed.next();
      this.marksChanged.next(true);
    }

  updateItem(item: Item, favUpdated?: boolean): void {
    if (item.mark == null) {
      item.markClass = null;
      item.markLabel = null;
      delete this.currentMarks.marked[item.id];
    }
    else {

      if (item.mark == "upgrade") {
        item.markClass = "info";
        item.markLabel = "Upgrade Me";
      }
      else if (item.mark == "keep") {
        item.markClass = "success";
        item.markLabel = "Keep Me";
      }
      else if (item.mark == "infuse") {
        item.markClass = "warning";
        item.markLabel = "Infusion Fuel";
      }
      else if (item.mark == "junk") {
        item.markClass = "danger";
        item.markLabel = "Dismantle Me";
      }
      else {
        console.log("Ignoring mark: " + item.mark);
        item.mark == null;
        return;
      }
      this.currentMarks.marked[item.id] = item.mark;
    }
    if (item.notes == null) {
      delete this.currentMarks.notes[item.id];
    }
    else {
      this.currentMarks.notes[item.id] = item.notes;
    }
    if (item.fav === null || item.fav === false) {
      delete this.currentMarks.favs[item.id];
    }
    else {
      this.currentMarks.favs[item.id] = true;
    }
    this.dirty = true;
    if (favUpdated) this.favsUpdatedFeed.next();
    this.dirtyFeed.next();
    this.marksChanged.next(true);
  }

  init(platform: number, memberId: string): Promise<boolean> {
    let self: MarkService = this;
    return this.loadMarks(platform, memberId).then(
      function (marks: Marks) {
        self.currentMarks = marks;
        self.dirty = false;
        return true;
      }
    )
  }

  private static processFavs(m: any, items: Item[]): boolean {
    let unusedDelete: boolean = false;
    let usedKeys: any = {};
    let totalKeys: number = 0, missingKeys: number = 0;
    for (var key in m) {
      if (m.hasOwnProperty(key)) {
        usedKeys[key] = false;
        totalKeys++;
      }
    }
    items.forEach(function (item: Item) {
      let mark: any = m[item.id];
      if (mark != null && mark === true) {
        item.fav = mark;
        usedKeys[item.id] = true;
      }
    });

    for (var key in usedKeys) {
      if (usedKeys.hasOwnProperty(key)) {
        //if it wasn't found, it's left over and should be deleted
        if (usedKeys[key] === false) {
          console.log("Deleting unused key: " + key);
          delete m[key];
          unusedDelete = true;
          missingKeys++;
        }
      }
    }
    console.log("Favs: " + missingKeys + " unused out of total " + totalKeys);
    return unusedDelete;
  }

  private static  processNotes(m: any, items: Item[]): boolean {
    let unusedDelete: boolean = false;
    let usedKeys: any = {};
    let totalKeys: number = 0, missingKeys: number = 0;
    for (var key in m) {
      if (m.hasOwnProperty(key)) {
        usedKeys[key] = false;
        totalKeys++;
      }
    }
    items.forEach(function (item: Item) {
      let mark: any = m[item.id];
      if (mark != null && mark.trim().length > 0) {
        item.notes = mark;
        usedKeys[item.id] = true;
      }
    });

    for (var key in usedKeys) {
      if (usedKeys.hasOwnProperty(key)) {
        //if it wasn't found, it's left over and should be deleted
        if (usedKeys[key] === false) {
          console.log("Deleting unused key: " + key);
          delete m[key];
          unusedDelete = true;
          missingKeys++;
        }
      }
    }
    console.log("Notes: " + missingKeys + " unused out of total " + totalKeys);
    return unusedDelete;
  }


  private static processMarks(m: any, items: Item[]): boolean {
    let unusedDelete: boolean = false;
    let usedKeys: any = {};
    let totalKeys: number = 0, missingKeys: number = 0;
    for (var key in m) {
      if (m.hasOwnProperty(key)) {
        usedKeys[key] = false;
        totalKeys++;
      }
    }
    items.forEach(function (item: Item) {

      if (item.vendor!==null){
        item.markClass = "active";
        item.markLabel = "Vendor";
        item.mark = "vendor";
        return;
      }
      let mark: any = m[item.id];
      if (mark != null && mark.trim().length > 0) {

        if (mark == "upgrade") {
          item.markClass = "info";
          item.markLabel = "Upgrade Me";
          item.mark = mark;
        }
        else if (mark == "keep") {
          item.markClass = "success";
          item.markLabel = "Keep Me";
          item.mark = mark;
        }
        else if (mark == "infuse") {
          item.markClass = "warning";
          item.markLabel = "Infusion Fuel";
          item.mark = mark;
        }
        else if (mark == "junk") {
          item.markClass = "danger";
          item.markLabel = "Dismantle Me";
          item.mark = mark;
        }
        else {
          console.log("Ignoring mark: " + mark);
          return;
        }
        usedKeys[item.id] = true;
      }
    });

    for (var key in usedKeys) {
      if (usedKeys.hasOwnProperty(key)) {
        //if it wasn't found, it's left over and should be deleted
        if (usedKeys[key] === false) {
          console.log("Deleting unused key: " + key);
          delete m[key];
          unusedDelete = true;
          missingKeys++;
        }
      }
    }
    console.log("Marks: " + missingKeys + " unused out of total " + totalKeys);
    return unusedDelete;
  }

  processItems(items: Item[]): void {
    let updatedMarks: boolean = MarkService.processMarks(this.currentMarks.marked, items);
    let updatedNotes: boolean = MarkService.processNotes(this.currentMarks.notes, items);
    let updatedFavs: boolean = MarkService.processFavs(this.currentMarks.favs, items);
    this.dirty = this.dirty || updatedFavs || updatedNotes || updatedMarks;
  }

  private loadMarks(platform: number, memberId: string): Promise<Marks> {
    let self: MarkService = this;
    let opts: RequestOptions = new RequestOptions(
      {
        method: RequestMethod.Get,
        responseType: ResponseContentType.Json
      });

    return self.http.get(environment.markUrl + platform + "/" + memberId, opts)
      .map(
        function (res) {
          let j: any = res.json();
          if (j.memberId != null) {
            return new Marks(platform, memberId, j.marked, j.notes, j.favs, j.todo);
          }
          return new Marks(platform, memberId, {}, {}, {}, {});
        }).toPromise().catch(
        function (err) {
          console.log("Error getting marks");
          console.dir(err);
          self.notificationService.fail("Failed to load marks: " + err.message);
          return new Marks(platform, memberId, {}, {}, {}, {});
        });
  }

  public saveMarks(): Promise<void> {
    let saveMe: any = {
      marked: this.currentMarks.marked,
      notes: this.currentMarks.notes,
      favs: this.currentMarks.favs,
      todo: this.currentMarks.todo,
      magic: "this is magic!",
      platform: this.currentMarks.platform,
      memberId: this.currentMarks.memberId
    };

    let s: string = JSON.stringify(saveMe);
    let lzSaveMe: string = LZString.compressToBase64(s);


    let self: MarkService = this;
    let opts: RequestOptions = new RequestOptions(
      {
        method: RequestMethod.Post,
        responseType: ResponseContentType.Json
      });

    return self.http.post(environment.markUrl,
      {
        data: lzSaveMe
      },
      opts)
      .map(
        function (res) {
          let obj: any = res.json();
          if (obj.status && obj.status === "success") {
            self.dirty = false;
            self.dirtyFeed.next();
          }
        }
      ).toPromise();

  }

}
