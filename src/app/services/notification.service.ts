/**
 * Created by Dave on 12/21/2016.
 */

import {Injectable} from '@angular/core';

import {Observable, Subject} from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

type Options = "success" | "error" | "info";

export interface Notification {
  mode: Options;
  title: string;
  message: string;

}

@Injectable()
export class NotificationService {
  private notifySub = new Subject();
  private statusSub = new Subject();
  private popubSub = new Subject();
  private thinkingSub = new Subject();
  public notifyFeed: Observable<Notification>;
  public statusFeed: Observable<string>;
  public popupFeed: Observable<boolean>;
  public thinkingFeed: Observable<boolean>;

  constructor() {
    this.notifyFeed = this.notifySub.asObservable() as Observable<Notification>;
    this.statusFeed = this.statusSub.asObservable() as Observable<string>;
    this.popupFeed = this.popubSub.asObservable() as Observable<boolean>;
    this.thinkingFeed = this.thinkingSub.asObservable() as Observable<boolean>;
  }

  public thinking(show: boolean): void{
    this.thinkingSub.next(show);
  }

  private notify(note:Notification){
    this.notifySub.next(note);
  }

  public popup(show: boolean): void{
    this.popubSub.next(show);
  }

  public updateStatus(msg: string): void{
    this.statusSub.next(msg);
  }

  public fail(msg: string): void {
    this.notify({
      mode: "error",
      title: "Error",
      message: msg
    });
    return;
  }

  public success(msg: string): void {
    this.notify({
      mode: "success",
      title: "Success",
      message: msg
    });
  }


  public info(msg: string): void {
    this.notify({
      mode: "info",
      title: "Info",
      message: msg
    });
  }
}
