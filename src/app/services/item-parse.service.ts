/**
 * Created by Dave on 12/21/2016.
 */
import {Injectable} from "@angular/core";
import {DestinyCacheService} from "./destiny-cache.service";
import {Step, StepGroup, Progression} from "./model/progression";
import {Item, Bounty, Weapon, Armor, Engram, Subclass, Quality, DamageType, Objective, Cost} from "./model/item";
import {max} from "rxjs/operator/max";
import {Http} from "@angular/http";
import {Character} from "./model/character";

const SUBCLASS_BUCKET: number = 3284755031;

export enum ItemType {
  Armor = 2,
  Weapon = 3,
  Engram = 8
}

class MinMax {
  readonly min: number;
  readonly max: number;

  constructor(min: number, max: number) {
    this.min = min;
    this.max = max;
  }
}

@Injectable()
export class ItemParseService {

  private blessedWeapons: any = null;

  constructor(private http: Http, private destinyCacheService: DestinyCacheService) {

    this.http.get("/assets/blessedWeapons.json?v=1")
      .map(
        res => {
          return res.json();
        }
      ).toPromise()
      .then(
        obj => {
          this.blessedWeapons = obj;
        }
      )
      .catch(
        err => {
          console.dir(err);
        }
      );
  }

  private static isHidden(stepName: string): boolean {
    if (stepName == null) return true;
    if (stepName === "Infuse") return true;
    if (stepName.startsWith("Reforge")) return true;
    if (stepName === "Increase Discipline") return true;
    if (stepName === "Increase Strength") return true;
    if (stepName === "Increase Intellect") return true;
    if (stepName === "Upgrade Damage") return true;
    if (stepName === "Twist Fate") return true;
    if (stepName === "Kinetic Damage") return true;
    return false;
  }

  private buildEmptyProgression() {
    return new Progression([], false, false, false, false, false, false, 1, 1, null, null);
  }

  private buildProgression(talentGridHash: number, progressionHash: number, currentXP: number, nodes: any[]): Progression {
    if (nodes == null) return;
    let tDesc: any = this.destinyCacheService.cache.TalentGrid[talentGridHash];
    if (tDesc == null) return;

    let armorDmgType: DamageType = null;
    let hasOrnament: boolean = false;

    //make dictionary of the nodes selected in this item
    let selectedNodes: any = {};
    for (let cntr: number = 0; cntr < nodes.length; cntr++) {
      selectedNodes[nodes[cntr].nodeHash] = nodes[cntr];
    }
    //iterate through all the archetypal nodes

    let groups: Step[][] = [];
    let activeIntBonus: boolean = false, activeStrBonus: boolean = false, activeDisBonus: boolean = false;
    let hasI: boolean = false, hasD: boolean = false, hasS: boolean = false;

    let maxGridLevel: number = 0;

    for (let cntr: number = 0; cntr < tDesc.nodes.length; cntr++) {
      let node: any = tDesc.nodes[cntr];
      //is this archetypal node actually on the instance?
      let selNode: any = selectedNodes[node.nodeHash];
      //if not, skip
      if (selNode == null) continue;
      //get the step of the node selected on the instance
      let step: any = node.steps[selNode.stepIndex];

      if (step.activationRequirement && step.activationRequirement.gridLevel) {
        if (step.activationRequirement.gridLevel > maxGridLevel) {
          maxGridLevel = step.activationRequirement.gridLevel;
        }
      }
      if (step.nodeStepName == null) continue;
      if (step.nodeStepName.length == 0) continue;
      //check to see if they have I/D/S activated for quality calcs later
      if (ItemParseService.isHidden(step.nodeStepName)) {
        if (step.nodeStepName === "Increase Discipline") {
          hasD = true;
          activeDisBonus = selNode.isActivated;
        }
        else if (step.nodeStepName === "Increase Strength") {
          hasS = true;
          activeStrBonus = selNode.isActivated;
        }
        else if (step.nodeStepName === "Increase Intellect") {
          hasI = true;
          activeIntBonus = selNode.isActivated;
        }
        continue;
      }
      if (step.nodeStepDescription.startsWith("Adorn")) continue;
      if (step.nodeStepDescription.startsWith("Applies an Ornament")) {
        hasOrnament = true;
        continue;
      }
      if (step.nodeStepName === "Arc Armor") armorDmgType = DamageType.Arc;
      if (step.nodeStepName === "Solar Armor") armorDmgType = DamageType.Solar;
      if (step.nodeStepName === "Void Armor") armorDmgType = DamageType.Void;

      let oStep: Step = new Step(
        node.column,
        step.nodeStepName,
        step.nodeStepDescription,
        selNode.isActivated,
        ItemParseService.isHidden(step.nodeStepName));

      if (groups[node.column] == null) {
        groups[node.column] = [];
      }
      groups[node.column].push(oStep);
    }
    let stepGroups: StepGroup[] = [];
    groups.forEach(
      function (steps: Step[]) {
        if (steps.length > 0)
          stepGroups.push(new StepGroup(steps));
      }
    )

    let progDef: any = this.destinyCacheService.cache.Progression[progressionHash];

    let totalXPRequired: number = 0;

    for (let cntr: number = 0; cntr < maxGridLevel; cntr++) {
      totalXPRequired += progDef.steps[Math.min(cntr, progDef.steps.length - 1)].progressTotal;
    }

    return new Progression(stepGroups, hasI, hasD, hasS, activeIntBonus, activeDisBonus, activeStrBonus, currentXP, totalXPRequired, armorDmgType, hasOrnament);
  }

  private static fitValue(light): number {
    if (light > 300) {
      return (0.2546 * light) - 23.825;
    }
    if (light > 200) {
      return (0.1801 * light) - 1.4612;
    } else {
      return -1;
    }
  }

  private static getScaledStat(base: number, light: number): MinMax {
    if (base === 0) {
      return new MinMax(0, 0);
    }

    var max = 335;
    if (light > 335) {
      light = 335;
    }
    const fit = ItemParseService.fitValue(max) / ItemParseService.fitValue(light);
    const calcMin: number = Math.floor((base) * fit);
    const calcMax: number = Math.floor((base + 1) * fit);

    return new MinMax(calcMin, calcMax);
  }

  private static getSplit(bucketName: string): number {
    if (bucketName == null) return null;
    switch (bucketName.toLowerCase()) {
      case 'helmet':
        return 46; // bungie reports 48, but i've only seen 46
      case 'gauntlets':
        return 41; // bungie reports 43, but i've only seen 41
      case 'chest':
      case 'chest armor':
        return 61;
      case 'leg':
      case 'leg armor':
        return 56;
      case 'classitem':
      case 'class items':
      case 'class armor':
      case 'ghost':
      case 'ghosts':
        return 25;
      case 'artifact':
      case 'artifacts':
        return 38;
      default:
        return null;
    }
  }

  private static getBonus(light: number, bucketName: string): number {
    switch (bucketName.toLowerCase()) {
      case 'helmet':
      case 'helmets':
        return light < 292 ? 15
          : light < 307 ? 16
            : light < 319 ? 17
              : light < 332 ? 18
                : 19;
      case 'gauntlets':
        return light < 287 ? 13
          : light < 305 ? 14
            : light < 319 ? 15
              : light < 333 ? 16
                : 17;
      case 'chest':
      case 'chest armor':
        return light < 287 ? 20
          : light < 300 ? 21
            : light < 310 ? 22
              : light < 319 ? 23
                : light < 328 ? 24
                  : 25;
      case 'leg':
      case 'leg armor':
        return light < 284 ? 18
          : light < 298 ? 19
            : light < 309 ? 20
              : light < 319 ? 21
                : light < 329 ? 22
                  : 23;
      case 'classitem':
      case 'class items':
      case 'class armor':
      case 'ghost':
      case 'ghosts':
        return light < 295 ? 8
          : light < 319 ? 9
            : 10;
      case 'artifact':
      case 'artifacts':
        return light < 287 ? 34
          : light < 295 ? 35
            : light < 302 ? 36
              : light < 308 ? 37
                : light < 314 ? 38
                  : light < 319 ? 39
                    : light < 325 ? 40
                      : light < 330 ? 41
                        : 42;
    }
    console.warn('item bonus not found', bucketName);
    return 0;
  }

  private static buildQuality(int: number, dis: number, str: number, light: number, bucketName: string, progression: Progression): Quality {
    if (light < 280) return null;
    const split: number = ItemParseService.getSplit(bucketName);
    if (split == null) {
      return null;
    }
    const bonus: number = ItemParseService.getBonus(light, bucketName);

    let strBonus: number = 0, disBonus: number = 0, intBonus: number = 0;
    if (progression.activeIntBonus) intBonus = bonus;
    if (progression.activeDisBonus) disBonus = bonus;
    if (progression.activeStrBonus) strBonus = bonus;

    let statSplit: string;

    if (progression.hasI && progression.hasD)
      statSplit = "ID";
    else if (progression.hasD && progression.hasS)
      statSplit = "DS";
    else if (progression.hasS && progression.hasI)
      statSplit = "IS";
    else
      statSplit = "";

    //max = split * 2
    let workingMin: number = 0;
    let workingMax: number = 0;

    let baseInt: number = int - intBonus;
    let intScaled: MinMax = ItemParseService.getScaledStat(baseInt, light);
    let minIntQ = Math.round(100 * intScaled.min / split);
    //let maxIntQ = Math.round(100 * intScaled.max / split);
    workingMin += intScaled.min;
    workingMax += intScaled.max;

    let baseDis = dis - disBonus;
    let disScaled: MinMax = ItemParseService.getScaledStat(baseDis, light);
    let minDisQ = Math.round(100 * disScaled.min / split);
    //let maxDisQ = Math.round(100 * disScaled.max / split);
    workingMin += disScaled.min;
    workingMax += disScaled.max;

    let baseStr = str - strBonus;
    let strScaled: MinMax = ItemParseService.getScaledStat(baseStr, light);
    let minStrQ = Math.round(100 * strScaled.min / split);
    //let maxStrQ = Math.round(100 * strScaled.max / split);
    workingMin += strScaled.min;
    workingMax += strScaled.max;

    const trueMax: number = split * 2;

    let quality: MinMax = new MinMax(
      Math.round(workingMin / trueMax * 100),
      Math.round(workingMax / trueMax * 100)
    );
    return new Quality(quality.min, minIntQ, minDisQ, minStrQ, statSplit);
  }


  private static getBlessedScore(data: any, name: string, prog: Progression, isPvp: boolean): number {
    let blessedScore = -1;

    if (data[name]) {
      let blessedPerkCols: any = data[name].perks;
      blessedScore = 0;
      let progColOffset:number = 0;
      if (prog.cols.length>4) progColOffset = 1;

      for (let cntr: number = 0; cntr < 4; cntr++) {
        if (blessedPerkCols.length < cntr) continue;
        if ((prog.cols.length) < (cntr+progColOffset)) continue;
        let blessedPerkNames: string[] = blessedPerkCols[cntr];
        let perks: Step[] = prog.cols[cntr+progColOffset].steps;
        let found: boolean = false;
        for (let cntr2 = 0; cntr2 < perks.length; cntr2++) {
          if (blessedPerkNames.indexOf(perks[cntr2].name) > -1) {
            if (isPvp)
              perks[cntr2].blessedPvp = true;
            else
              perks[cntr2].blessedPve = true;
            found = true;
            //break;
          }
        }
        if (found) {
          blessedScore++;
        }
      }
    }
    return blessedScore;
  }

  public parseBounty(forSale: boolean, rawItm: any, type: string): Bounty{
    let self: ItemParseService = this;
    const hash = rawItm.itemHash;
    const jDesc: any = this.destinyCacheService.cache.InventoryItem[hash];
    if (jDesc == null) return null;
    let objs: Objective[] = [];
    rawItm.objectives.forEach(function(obj){
      if (obj.objectiveHash){
        let jObj: any = self.destinyCacheService.cache.Objective[obj.objectiveHash];
        if (jObj){
          let oObj: Objective = new Objective(jObj.displayDescription, obj.progress, jObj.completionValue, jObj.isCountingDownward);
          objs.push(oObj);
        }
      }
    });
    return new Bounty(hash, forSale, jDesc.itemName, jDesc.itemTypeName, jDesc.icon, jDesc.itemDescription, objs);
  }

  public parseCosts(saleItem:any): Cost[]{
    let self: ItemParseService = this;
    if (saleItem==null) return [];
    if (saleItem.costs==null) return [];
    let costs: Cost[] = [];
    saleItem.costs.forEach(function(entry){
      const jDesc: any = self.destinyCacheService.cache.InventoryItem[entry.itemHash];
      if (jDesc==null){
        console.log(entry.itemHash+" is null");
        debugger;
      }
      costs.push(new Cost(jDesc.itemName, entry.value));
    });
    return costs;
  }


  public parse(rawItm: any, bucketHash: Number): Item {

    const hash = rawItm.itemHash;
    const jDesc: any = this.destinyCacheService.cache.InventoryItem[hash];
    if (jDesc == null) return null;

    if (bucketHash==null){
      bucketHash = jDesc.bucketTypeHash;
    }

    //2 = armor, 3 = weapon, 8 = engram
    //4 crucible bounty, 0 weekly elite bounty
    let isSubclass: boolean = jDesc.itemType === 0 && bucketHash === SUBCLASS_BUCKET;

    if (jDesc.itemType===0 && jDesc.values && (jDesc.values[2463743657]!=null || jDesc.values[2586866494]!=null)){
      return this.parseBounty(false, rawItm, "Zavala");
    }
    else if (jDesc.itemType===4 && jDesc.values && jDesc.values[3747303650]!=null){
      return this.parseBounty(false, rawItm, "Shaxx");
    }

    if (!isSubclass && jDesc.itemType != ItemType.Armor && jDesc.itemType != ItemType.Weapon && jDesc.itemType != ItemType.Engram) return null;

    const bucketName: string = this.destinyCacheService.cache.InventoryBucket[jDesc.bucketTypeHash].bucketName;
    const locked: boolean = rawItm.locked;
    const id: string = rawItm.itemInstanceId;

    if (jDesc.itemType === ItemType.Engram) {
      return new Engram(jDesc.itemName, jDesc.itemTypeName, 0, jDesc.icon,
        bucketName, jDesc.itemType, jDesc.tierTypeName, jDesc.classType, null, locked, id, hash);
    }

    let progression: Progression = null;

    if (rawItm.progression == null) {
      progression = this.buildEmptyProgression();
    }
    else {
      progression = this.buildProgression(rawItm.talentGridHash, rawItm.progression.progressionHash,
        rawItm.progression.currentProgress, rawItm.nodes);
    }


    if (jDesc.itemType === ItemType.Armor) {
      let int: number = 0, dis: number = 0, str: number = 0;
      for (let cntr6: number = 0; cntr6 < rawItm.stats.length; cntr6++) {

        if (rawItm.stats[cntr6].statHash === 144602215)
          int = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 1735777505)
          dis = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 4244567218)
          str = rawItm.stats[cntr6].value;
      }
      const light: number = rawItm.primaryStat ? rawItm.primaryStat.value : 0;
      let quality: Quality = ItemParseService.buildQuality(int, dis, str, light, bucketName, progression);

      return new Armor(jDesc.itemName, jDesc.itemTypeName, light, jDesc.icon,
        bucketName, jDesc.itemType, jDesc.tierTypeName, jDesc.classType, progression,
        quality, int, dis, str, locked, id, hash, rawItm.isEquipped, progression.armorDmgType, jDesc.itemDescription);
    }
    else if (jDesc.itemType === ItemType.Weapon) {
      let impact: number = 0, range: number = 0, stability: number = 0, rof: number = 0, reload: number = 0, magazine: number = 0;
      let velocity: number, blastRadius: number, chargeRate: number, speed: number, defense: number, energy: number, efficiency: number;
      for (let cntr6: number = 0; cntr6 < rawItm.stats.length; cntr6++) {
        if (rawItm.stats[cntr6].statHash === 1240592695)
          range = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 2523465841) //velocity rocket launcher
          velocity = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 4043523819)
          impact = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 3614673599) //blast radius rocket launcher
          blastRadius = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 155624089)
          stability = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 4284893193)
          rof = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 2961396640) //charge rate, fusion
          chargeRate = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 2837207746) //speed, sword
          speed = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 4188031367)
          reload = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 209426660) //defense, sword
          defense = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 3871231066)
          magazine = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 925767036) //Energy, Sword
          energy = rawItm.stats[cntr6].value;
        else if (rawItm.stats[cntr6].statHash === 2762071195) { //Efficiency, Sword
          efficiency = rawItm.stats[cntr6].value;
        }
        else {
          let statHash: number = rawItm.stats[cntr6].statHash;
          console.log(jDesc.itemName + " " + statHash + " " + this.destinyCacheService.cache.Stat[statHash].statName);
        }
      }
      let aa: number, inv: number, optics: number, recoil: number;

      let jo: any = jDesc.stats[1345609583];
      if (jo != null) aa = jo.value;
      jo = jDesc.stats[1931675084];
      if (jo != null) inv = jo.value;
      jo = jDesc.stats[3555269338];
      if (jo != null) optics = jo.value;
      jo = jDesc.stats[2715839340];
      if (jo != null) recoil = jo.value;


      let pveBlessedScore = -1;
      let pvpBlessedScore = -1;
      if (this.blessedWeapons){
        pveBlessedScore = ItemParseService.getBlessedScore(this.blessedWeapons.pve, jDesc.itemName, progression, false);
        pvpBlessedScore = ItemParseService.getBlessedScore(this.blessedWeapons.pvp, jDesc.itemName, progression, true);
      }


      return new Weapon(jDesc.itemName, jDesc.itemTypeName, rawItm.primaryStat ? rawItm.primaryStat.value : 0, jDesc.icon,
        bucketName, jDesc.itemType, jDesc.tierTypeName, jDesc.classType, progression,
        impact, range, stability, rof, reload, magazine, velocity, blastRadius, chargeRate, speed, defense, energy, efficiency,
        aa, inv, optics, recoil,
        rawItm.damageType, locked, id, hash, rawItm.isEquipped, pveBlessedScore, pvpBlessedScore, jDesc.itemDescription);
    }

    else if (isSubclass) {
      return new Subclass(jDesc.itemName, jDesc.itemTypeName, 0, jDesc.icon,
        bucketName, jDesc.itemType, jDesc.tierTypeName, jDesc.classType, progression, locked, id, hash,
        rawItm.isEquipped);
    }
    else
      return null;
  }
}
