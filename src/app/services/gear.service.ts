/**
 * Created by Dave on 12/23/2016.
 */
import {Injectable} from "@angular/core";
import {NotificationService} from "./notification.service";
import {DestinyCacheService} from "./destiny-cache.service";
import {ItemParseService} from "./item-parse.service";
import {MarkService} from "./mark.service";
import {BungieService} from "./bungie.service";
import {AuthService} from "./auth.service";
import {Player, Platform} from "./model/player";
import {Character, Target, Vault, Vendor} from "./model/character";
import {Item, Weapon, Armor, Engram, Subclass, Bounty, Cost} from "./model/item";
import {BucketService, Bucket} from "./bucket.service";
import {Subject} from "rxjs";


@Injectable()
export class GearService {
  public player: Player = null;
  public chars: Character[] = [];
  public vault: Vault = new Vault();
  public weapons: Weapon[] = [];
  public armor: Armor[] = [];
  public engrams: Engram[] = [];
  refreshedFeed: Subject<boolean> = new Subject<boolean>();

  constructor(private authService: AuthService, private notificationService: NotificationService,
              private destinyCacheService: DestinyCacheService,
              private bungieService: BungieService,
              private itemParseService: ItemParseService,
              private markService: MarkService,
              private bucketService: BucketService) {
  }

  private parseBuckets(buckets: any, invisible?: boolean): Item[] {
    let items: Item[] = [];
    let self: GearService = this;
    if (buckets) {
      buckets.forEach(function (bucket: any) {
        if (bucket.items) {
          bucket.items.forEach(
            function (parseMe) {
              let item: Item = self.itemParseService.parse(parseMe, bucket.bucketHash);
              if (item != null) {
                if (true==invisible){
                  item.postmaster = true;
                }
                items.push(item);
              }

            }
          );
        }
      });
    }
    return items;
  }

  private getVault(): Promise<Item[]> {
    let self: GearService = this;
    return this.bungieService.getVault(this.player).then(
      function (data: any) {
        const allItems: Item[] = self.parseBuckets(data.buckets);
        allItems.forEach(function (item) {
          if (item.postmaster!=true) {
            item.owner = self.vault;
            item.options = self.chars.slice(0);
          }
        });
        return allItems;
      }
    );
  }

  private setLockState(itm: Item, locked: boolean): Promise<boolean> {

    let self: GearService = this;
    let target: Character;
    //Bungie requires a char id, even for value items, weird
    if (itm.owner instanceof Vault) {
      target = this.chars[0];
    }
    else {
      target = itm.owner as Character;
    }
    return this.bungieService.setLockState(this.player, target, itm, locked).then(
      function (data) {
        itm.locked = locked;
        if (locked)
          self.notificationService.success("Locked " + itm.name);
        else
          self.notificationService.success("Unlocked " + itm.name);
        return true;

      }
    ).catch(function (err) {
      console.dir(err);
      self.notificationService.fail(err.message);
      return false;

    });
  }

  unlock(itm: Item) {
    this.setLockState(itm, false);
  }


  lock(itm: Item) {
    this.setLockState(itm, true);
  }

  private fixLocks(itms: Item[]) {
    let fixMe: any[] = [];
    itms.forEach(
      i => {
        if (i.mark == "upgrade" || i.mark == "keep") {
          if (!i.locked) {
            fixMe.push({
              item: i,
              lock: true
            });
          }
        }
        else if (i.mark == "junk" || i.mark == "infuse") {

          if (i.locked) {
            fixMe.push({
              item: i,
              lock: false
            });
          }
        }
      }
    )
    this.notificationService.info("Fixing lock status on "+fixMe.length+" items");
    console.dir(fixMe);

    let sub: Subject<boolean> = new Subject<boolean>();
    sub.subscribe(success =>
    {
      if (fixMe.length>0){
        let fix: any = fixMe.pop();
        this.setLockState(fix.item, fix.lock).then(
          function(success){
            if (success)
            sub.next(true);
          }
        );
      }
      else{
        this.notificationService.info("Done updating locks!");
      }
    });
    sub.next(true);
  }

  fixWeaponLocks() {
    this.fixLocks(this.weapons);
  }

  fixArmorLocks(){
    this.fixLocks(this.armor);
  }

  equip(item: Item): Promise<boolean> {
    let self: GearService = this;
    let prom: Promise<any>;
    prom = this.bungieService.equip(this.player, item.owner as Character, item);
    prom.then(
      function (data) {
        let bucket: Bucket = self.bucketService.getBucket(item.owner, item.bucketName);
        let oldEquipped = bucket.equipped;
        oldEquipped.equipped = false;
        item.equipped = true;
        bucket.equipped = item;
        return true;
      }
    );
    prom.catch(function (err) {
      console.dir(err);
      self.notificationService.fail(err.message);
      return false;
    });
    return prom;
  }

  transfer(itm: Item, target: Target){

    let self: GearService = this;

    let equipProm: Promise<boolean> = Promise.resolve(true);
    if (itm.equipped){
      let equipMe: Item = self.bucketService.getBucket(itm.owner, itm.bucketName).otherItem(itm);
      if (equipMe==null){
        throw new Error("Nothing to equip to replace "+itm.name);
      }
      equipProm = self.equip(equipMe);
      //add command to equip something else
    }

    equipProm.then(
      function(blah){
        let prom: Promise<any>;
        if (target instanceof Vault) {
          prom = self.bungieService.transfer(self.player, itm.owner as Character, itm, true);
        }
        else {
          if (itm.owner instanceof Vault) {
            prom = self.bungieService.transfer(self.player, target as Character, itm, false);
          }
          //send to vault, then to char
          else {
            prom = self.bungieService.transfer(self.player, itm.owner as Character, itm, true).then(
              function (data) {
                itm.options.push(itm.owner);
                itm.owner = self.vault;
                itm.options.splice(itm.options.indexOf(self.vault), 1);
                //self.notificationService.success("Moved "+itm.name+" to "+target.label);
                return self.bungieService.transfer(self.player, target as Character, itm, false);
              }
            );

          }
        }
        prom.then(
          function (data) {
            itm.options.push(itm.owner);
            itm.owner = target;
            itm.options.splice(itm.options.indexOf(target), 1);
            //self.notificationService.success("Moved "+itm.name+" to "+target.label);
          }
        );
        prom.catch(function (err) {
          console.dir(err);
          self.notificationService.fail(err.message);

        });



      }
    );

  }


  transfer3(itm: Item, target: Target) {

    //if item is equipped, equip something else
    //if target is vault, transfer to vault
    //if item is in vault, send to player
    //if item is on player, send to vault, then to player

    let self: GearService = this;
    let prom: Promise<any>;
    if (target instanceof Vault) {
      prom = this.bungieService.transfer(this.player, itm.owner as Character, itm, true);
    }
    else {
      if (itm.owner instanceof Vault) {
        prom = this.bungieService.transfer(this.player, target as Character, itm, false);
      }
      //send to vault, then to char
      else {
        prom = this.bungieService.transfer(this.player, itm.owner as Character, itm, true).then(
          function (data) {
            itm.options.push(itm.owner);
            itm.owner = self.vault;
            itm.options.splice(itm.options.indexOf(self.vault), 1);
            //self.notificationService.success("Moved "+itm.name+" to "+target.label);
            return self.bungieService.transfer(self.player, target as Character, itm, false);
          }
        );

      }
    }
    prom.then(
      function (data) {
        itm.options.push(itm.owner);
        itm.owner = target;
        itm.options.splice(itm.options.indexOf(target), 1);
        //self.notificationService.success("Moved "+itm.name+" to "+target.label);
      }
    );
    prom.catch(function (err) {
      console.dir(err);
      self.notificationService.fail(err.message);

    });
  }

  private getInventory(char: Character): Promise<Item[]> {
    //other options
    let self: GearService = this;

    let options: Target[] = [self.vault];
    self.chars.forEach(function (char1) {
      if (char1 !== char) {
        options.push(char1);
      }
    });

    return this.bungieService.getInventory(this.player, char).then(
      function (data: any) {
        const items1: Item[] = self.parseBuckets(data.buckets.Equippable);
        const items2: Item[] = self.parseBuckets(data.buckets.Invisible, true);
        const items3: Item[] = self.parseBuckets(data.buckets.Item);
        const allItems: Item[] = items1.concat(items2).concat(items3);
        allItems.forEach(function (item) {
          item.owner = char;
          if (item.postmaster!=true) {
            item.options = options.slice(0);
          }
        });
        return allItems;
      }
    );
  }

  // '3611686524', DO
  // '1821699360', FWC
  // '1808244981', NM
  // '2610555297', IB
  // '2796397637', Xur
  // '3003633346', hunter van
  // '1575820975', warlock van
  // '2668878854', vang quarter
  // 1990950  titan vang
  // '3658200622', cruc quart
  // 3746647075 cruc handler


  // x '1998812735', HOJ
  // x '2680694281', speaker
  // x '2190824860', shiro
  // x'174528503', eris
  // x'2190824863', cryptarch
  // x'3902439767', exotic blueprints
  // x'134701236', gaurd outfit
  // x'459708109' shipwright

  private vendorCache:any = {};

  public getAllVendorItems(char: Character): Promise<Item[]>{
    if (this.vendorCache[char.id]!=null && this.vendorCache[char.id].length>0){
      console.log("Using cached vendor items");
      return Promise.resolve(this.vendorCache[char.id]);
    }
    let self: GearService = this;
    let proms: Promise<Item[]>[] = [];
    proms.push(self.getVendorItems(char, "Dead Orbit", "3611686524"));
    proms.push(self.getVendorItems(char, "Future War Cult", "1821699360"));
    proms.push(self.getVendorItems(char, "New Monarchy", "1808244981"));
    proms.push(self.getVendorItems(char, "Xur", "2796397637"));
    proms.push(self.getVendorItems(char, "Cayde", "3003633346"));
    proms.push(self.getVendorItems(char, "Ikora", "1575820975"));
    proms.push(self.getVendorItems(char, "Zavala", "1990950"));
    proms.push(self.getVendorItems(char, "Crucible Quartermaster", "3658200622"));
    proms.push(self.getVendorItems(char, "Shaxx", "3746647075"));


    proms.push(self.getVendorItems(char, "Variks","1998812735"));
    proms.push(self.getVendorItems(char, "Speaker", "2680694281"));
    proms.push(self.getVendorItems(char, "Shiro", "2190824860"));
    proms.push(self.getVendorItems(char, "Eris", "174528503"));
    proms.push(self.getVendorItems(char, "Tyra Karn", "2190824863"));
    proms.push(self.getVendorItems(char, "Iron Banner", "2610555297"));
    proms.push(self.getVendorItems(char, "Exotic Kiosk", "3902439767"));


    return Promise.all(proms).then(
      values => {
        let reduced: Item[] = values.reduce(
          (a: Item[], b: Item[]) => {
            let c: Item[] = a.concat(b);
            return c;
          }
        );
        self.vendorCache[char.id] = reduced;
        return reduced;
      }
    ).catch(function(err){
      console.log("Ignoring vendor error: "+err);
      return [];
    });
  }

  private getVendorItems(char: Character, type: string, vendorId: string): Promise<Item[]> {
    let self: GearService = this;
    return this.bungieService.getVendor(this.player, char, vendorId).then(
      function (data: any) {

        let items: Item[] = [];
        if (data.saleItemCategories){
          data.saleItemCategories.forEach(function(cat){
            //Exotic Gear
            if (cat.categoryTitle!==null && (
              cat.categoryTitle==="Rank 1"
              || cat.categoryTitle==="Rank 2"
              || cat.categoryTitle==="Rank 3"
              || cat.categoryTitle==="Weapons"
              || cat.categoryTitle==="Crucible Weapons" || cat.categoryTitle==="Exotic Gear"
              || type==="Exotic Kiosk"
              || cat.categoryTitle.startsWith("Iron Lord Artifacts")
              || cat.categoryTitle.startsWith("Gear")
              || cat.categoryTitle.startsWith("Subclass")
              || cat.categoryTitle.startsWith("House of Judgment Gear")
              || cat.categoryTitle.startsWith("Equipment")
              || cat.categoryTitle.startsWith("Armor")
              || cat.categoryTitle.startsWith("Crota's Bane Rank 5")
              || cat.categoryTitle.startsWith("Event Rewards")
            ))
            {

              if (cat.saleItems){
                cat.saleItems.forEach(function(saleItem){
                  let item: Item = self.itemParseService.parse(saleItem.item, null);
                  if (item != null) {
                    item.id = ""+ item.hash + saleItem.vendorItemIndex;
                    item.postmaster = true;
                    item.owner = new Vendor();
                    item.costs = self.itemParseService.parseCosts(saleItem);
                    item.markClass = "active";
                    item.vendor = type;
                    item.options = [];
                    items.push(item);
                  }

                });
              }



            }
            else if (cat.categoryTitle==="Available Bounties"){
              if (cat.saleItems){
                cat.saleItems.forEach(function(saleItem){
                  let bty: Bounty = self.itemParseService.parseBounty(true, saleItem.item, type);
                  bty.owner = char;
                  items.push(bty);
                });
              }

            }
            else{
              //do nothing

            }
          });
        }
        return items;
      }
    ).catch(function(err){
      console.log("Ignoring vendor error ("+type+"): "+err);
      return [];

    });
  }

  public refreshAccount(): Promise<Character[]> {
    if (this.player == null) {
      console.log("Player is null?");
      return;
    }
    this.notificationService.updateStatus("Loading characters...");
    let self: GearService = this;
    return this.bungieService.getAccount(this.player).then(
      function (chars: Character[]) {
        self.chars = chars;
        return new Promise(function (resolve, reject) {
          let proms: Promise<Item[]>[] = chars.map(
            function (char) {
              return self.getInventory(char);
            }
          );
          proms.push(self.getVault());
          chars.forEach(char=>proms.push(self.getAllVendorItems(char)));

          Promise.all(proms).then(
            values => {
              return values.reduce(
                (a: Item[], b: Item[]) => {
                  return a.concat(b);
                }
              )
            }
          ).then(
            function (items: Item[]) {

              let dict:any = {};
              let d: Item[] = items.filter(function(i){
                if (dict[i.id]!=null){
                  return false;
                }
                else{
                  dict[i.id] = true;
                  return true;
                }
              });
              items = d;


              self.markService.processItems(items);
              self.bucketService.init(chars, self.vault, items);
              let weapons: Weapon[] = [];
              let armor: Armor[] = [];
              let engrams: Engram[] = [];
              let bounties: Bounty[] = [];
              items.forEach(function (item) {
                if (item instanceof Bounty){
                  bounties.push(item);
                }
                else if (item instanceof Weapon) {
                  weapons.push(item);
                }
                else if (item instanceof Armor) {
                  armor.push(item);
                }
                else if (item instanceof Engram) {
                  engrams.push(item);
                }
                else if (item instanceof Subclass) {
                  let subclasses: Subclass[] = (item.owner as Character).subclass;

                  subclasses.push(item);
                  if (item.equipped === true) {
                    (item.owner as Character).equippedSubclass = item;
                  }
                }
              });
              self.assignBounties(bounties);
              self.armor = armor;
              self.weapons = weapons;
              self.engrams = engrams;
              self.notificationService.updateStatus("");
              self.refreshedFeed.next(true);
              resolve(true);
            }
          ).catch(
            function (err) {
              reject(err);
            }
          );
        });
      }
    );
  }

  private assignBounties(bounties: Bounty[]){
    let self: GearService = this;

    let bountyList: any = {};

    self.chars.forEach(function(char){
      bountyList[char.id] = [];
    });

    bounties.forEach(function(bty){
      if (bty.forSale===false) {
        bountyList[bty.owner.id].push(bty);
      }
    });
    bounties.forEach(function(bty){
      if (bty.forSale===true) {
        let target: Bounty[] = bountyList[bty.owner.id];
        let found: boolean = false;
        for (let cntr:number=0; cntr<target.length; cntr++){
          if (target[cntr].hash === bty.hash){
            found = true;
            break;
          }
        }
        if (!found){
          bountyList[bty.owner.id].push(bty);
        }

      }
    });

    self.chars.forEach(function(char){
      bountyList[char.id].sort(function(a, b) {
        return a.pct < b.pct;
      });

      char.bounties = bountyList[char.id];
    });

  }

  public setPreferredPlatform(platform: Platform) {
    this.bungieService.setPreferredPlatform(platform);
  }

  public signOut() {
    this.authService.signOut();
  }

  public init(): Promise<Character[]> {
    let self: GearService = this;
    self.notificationService.updateStatus("Loading inventory database...");

    let authProm: Promise<boolean> = this.authService.init().then(
      function (key: string) {
        return (true);
      }
    ).catch(function (err) {
      console.dir(err);
      throw new Error(err.message);
    });

    let cacheProm: Promise<boolean> =
      this.destinyCacheService.init()
        .then(
          function (bSuccess: boolean) {
            return true;
          }
        )
        .catch(
          function (err) {
            console.dir(err);
            throw new Error("Inventory database failed to load");
          });


    let playerProm: Promise<boolean> = self.bungieService.getBungieNetUser().then(
      function (p: Player) {
        console.log("Setting player");
        self.player = p;
        return p;
      }).then(
      function (p: Player) {
        return self.markService.init(p.id.platform, p.id.id);
      });

    return Promise.all([authProm, cacheProm, playerProm]).then(
      function (data) {
        console.log("Refreshing account");
        return self.refreshAccount();
      }
    );
  }
}
