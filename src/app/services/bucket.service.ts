/**
 * Created by Dave on 12/21/2016.
 */
import {Injectable} from "@angular/core";

import {Character, Target, Vault, Vendor} from "./model/character";
import {Item, Weapon, Armor, Engram, Subclass} from "./model/item";

export class Bucket{
    equipped: Item;

    readonly items: Item[] = [];
    readonly name: string;

    constructor(name: string) {
        this.name = name;
    }

    otherItem(notMe: Item): Item{
      for (let cntr=0; cntr<this.items.length; cntr++){
        if (this.items[cntr]!=notMe){
          if (this.items[cntr].tierTypeName!="Exotic")
            return this.items[cntr];
        }
      }
      return null;
    }

}

@Injectable()
export class BucketService {

  constructor() {
  }

  private buckets: any;

  getBucket(target: Target, bucketName: string): Bucket{
      return this.buckets[target.id][bucketName];
  }

  init(chars: Character[], vault: Vault, items: Item[]){
      this.buckets = {};
      for (let cntr=0; cntr<chars.length; cntr++){
        this.buckets[chars[cntr].id] = {};
      }
      this.buckets[vault.id] = {};

      for (let cntr=0; cntr<items.length; cntr++){
          let itm: Item = items[cntr];
          if (!itm.bucketName || !itm.owner) continue;
          if (itm.owner instanceof Vendor) continue;
          let buckets: any = this.buckets[itm.owner.id];
          let bucket: Bucket = buckets[itm.bucketName];
          if (bucket==null){
              bucket = new Bucket(itm.bucketName);
              buckets[itm.bucketName] = bucket;
          }
          bucket.items.push(itm);
          if (itm.equipped){
              bucket.equipped = itm;
          }
      }
  }

}
