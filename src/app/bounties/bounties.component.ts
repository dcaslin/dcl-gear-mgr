import {Component, AfterViewInit, ViewChild} from "@angular/core";
import {GearService} from "../services/gear.service";
import {Character} from "../services/model/character";
import {TabsetComponent} from "ng2-bootstrap";

@Component({
  selector: 'bounties',
  styleUrls: ['./bounties.component.css'],
  templateUrl: './bounties.component.html'
})

export class BountiesComponent implements AfterViewInit {

  @ViewChild(TabsetComponent)
  private ts: TabsetComponent;

  chars: Character[] = [];

  constructor(private gearService: GearService) {

    this.clearTabs();
    this.chars = this.gearService.chars;
    this.gearService.refreshedFeed.subscribe(
      x => {
        this.clearTabs();
        this.chars = this.gearService.chars;
      }
    );
  }

  ngAfterViewInit() {
    this.chars = this.gearService.chars;
  }

  private clearTabs() {
    if (!this.ts) return;
    while (this.ts.tabs[0] != null) {
      this.ts.removeTab(this.ts.tabs[0])
    }
  }


}
