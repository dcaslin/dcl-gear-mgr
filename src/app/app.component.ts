import {Component, OnInit, ViewContainerRef, ViewChild, AfterViewInit} from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {NotificationService, Notification} from "./services/notification.service";
import {GearService} from "./services/gear.service";
import {MarkService} from "./services/mark.service";
import {Player, Platform} from "./services/model/player";
import {ToastsManager} from "ng2-toastr/ng2-toastr";
import {ModalDirective} from "ng2-bootstrap/modal";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  hamBurgerCollapsed: boolean = true;

  @ViewChild('loadModal')
  loadModal: ModalDirective;

  statusMsg: string = "Loading...";
  ready: boolean = false;
  thinking: boolean = false;
  marksDirty: boolean = false;

  saveMarks(): void {
    this.markService.saveMarks();
  }


  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private notificationService: NotificationService,
              private gearService: GearService,
              private markService: MarkService,
              public toastr: ToastsManager, vRef: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vRef);
    let self: AppComponent = this;
    this.notificationService.thinkingFeed.subscribe(
      x => {
        this.thinking = x;
      },
      err => console.log('Error: %s', err),
      () => console.log("Completed")
    );
    this.notificationService.notifyFeed.subscribe(
      function (x: Notification) {
        if (x.mode === "success") {
          self.toastr.success(x.message, x.title, {toastLife: 2000});
        }
        else if (x.mode === "info") {
          self.toastr.info(x.message, x.title);

        }
        else if (x.mode === "error") {
          self.toastr.error(x.message, x.title, {toastLife: 5000});
        }
      },
      err => console.log('Error: %s', err),
      () => console.log("Completed")
    );
    this.notificationService.statusFeed.subscribe(
      x => {
        this.statusMsg = x;
      },
      err => console.log('Error: %s', err),
      () => console.log("Completed")
    )

    this.notificationService.thinkingFeed.subscribe(
      x => {
        this.thinking = x;
      },
      err => console.log('Error: %s', err),
      () => console.log("Completed")
    );
    this.markService.dirtyFeed.subscribe(
      x => {
        this.marksDirty = this.markService.dirty;
        console.log("Marks dirty: " + this.marksDirty);
      }
    );
  }

  selectedPlayer(): Player {
    return this.gearService.player;
  }

  setPreferredPlatform(platform: Platform): void {
    this.gearService.setPreferredPlatform(platform);
  }

  signOut(): void {
    this.gearService.signOut();
  }

  refreshAccount(): void {
    let self: AppComponent = this;
    this.notificationService.popup(true);
    this.gearService.refreshAccount().catch(
      function (data) {
        //nothing
      }
    ).then(
      function (data) {
        self.notificationService.popup(false);
      }
    )
  }

  ngAfterViewInit() {

    this.loadModal.show();
    this.notificationService.popupFeed.subscribe(
      x => {
        if (x === true) {
          this.loadModal.show();
        }
        else {
          this.loadModal.hide();
        }
      },
      err => console.log('Error: %s', err),
      () => console.log("Completed")
    );
    (window as any).pushAds();
    return true;
  }

  ngOnInit() {


    //TODO 1990950 titan vanguard
    //TODO shaxx 3746647075

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .filter(event => event instanceof NavigationEnd)
      .subscribe(
        (navEnd: NavigationEnd) =>{
          (window as any).sendPageView(navEnd.urlAfterRedirects);
        }
      );

    let self: AppComponent = this;
    this.notificationService.popup(true);
    this.gearService.init().then(
      function (success) {
        self.ready = true;
      }).catch(
      function (err) {
        console.dir(err);
        self.notificationService.fail(err.message);
        self.notificationService.updateStatus("Failed loading: " + err.message);
        self.ready = false;
      }).then(
      function (data) {
        self.notificationService.popup(false);
        self.notificationService.success("Loaded!");
      }
    );
  }
}
