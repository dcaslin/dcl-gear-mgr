import {Component, Input, OnInit} from "@angular/core";
import {GearService} from "../services/gear.service";
import {Character} from "../services/model/character";
import {Subclass} from "../services/model/item";
import {Platform} from "../services/model/player";
import {MarkService, Todo} from "../services/mark.service";

@Component({
  selector: '[char]',
  styleUrls: ['./char.component.css'],
  templateUrl: './char.component.html'
})

export class CharComponent implements OnInit {
  todos: Todo[] = [];

  @Input()
  char: Character;

  constructor(private gearService: GearService, private markService: MarkService) {
  }

  ngOnInit() {
    let tempTodos: any[] = this.markService.todoItems(this.char.id);
    if (tempTodos.length == 0) {
      tempTodos.push(new Todo());
    }
    this.todos = tempTodos;
  }

  markTodoChanged() {
    this.markService.updateTodo(this.char.id, this.todos);
  }

  removeTodo(todo) {
    if (this.todos.length < 2) {
      if (this.todos.length == 1) {
        this.todos[0].text = "";
      }
      this.markTodoChanged();
      return;
    }
    let index: number = this.todos.indexOf(todo);
    if (index > -1) {
      this.todos.splice(index, 1);
    }
    this.markTodoChanged();
  }

  onTodoKey(e, todo) {
    if (e && e.keyCode === 13) {
      this.addTodo(todo);
    }
  }

  addTodo(todo) {
    if (this.todos.length > 10) return;
    if (todo == null) {
      this.todos.push(new Todo(true));
    }
    else {
      let index: number = this.todos.indexOf(todo);
      this.todos.splice(index + 1, 0, new Todo(true));
    }


    this.markTodoChanged();
  }

  buildTailorUrl(char: Character) {
    //http://destinytailor.com/xbox/e5%20dweezil/warlock
    if (this.gearService.player.id.platform == Platform.XBL) {
      return "http://destinytailor.com/xbox/" + encodeURIComponent(this.gearService.player.id.displayName) + "/" + char.className.toLowerCase();
    }
    else if (this.gearService.player.id.platform == Platform.PSN) {
      return "http://destinytailor.com/psn/" + encodeURIComponent(this.gearService.player.id.displayName) + "/" + char.className.toLowerCase();
    }
    return "";

  }

  equip(sub: Subclass) {
    this.gearService.equip(sub).then(
      (success) => {
        this.char.equippedSubclass = sub;
      }
    );
  }
}
