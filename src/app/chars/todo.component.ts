/**
 * Created by davecaslin on 2/23/17.
 */
import {Component, Input, Output, AfterViewInit, Renderer, ElementRef, EventEmitter} from "@angular/core";
import {Todo} from "../services/mark.service";

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html'
})

export class TodoComponent implements AfterViewInit {

  @Input() todo: Todo;
  @Input() last: boolean;
  @Input() empty: boolean;

  @Output() onChanged = new EventEmitter<Todo>();
  @Output() onAdd = new EventEmitter<Todo>();
  @Output() onRemove = new EventEmitter<Todo>();

  constructor(public renderer: Renderer, public elementRef: ElementRef) {

  }

  markTodoChanged() {
    this.onChanged.emit(this.todo);
  }


  onTodoKey(e) {
    if (e && e.keyCode === 13) {
      this.addTodo();
    }
  }

  addTodo() {
    this.onAdd.emit(this.todo);
  }


  removeTodo() {
    this.onRemove.emit(this.todo);
  }


  ngAfterViewInit() {
    if (this.todo.focusMe === true) {
      setTimeout(() => {
        //this.elementRef.nativeElement.querySelector('input').focus();
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('input'), 'focus', []);
        this.todo.focusMe = false;
      }, 1);
    }
  }

}
