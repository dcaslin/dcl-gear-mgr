import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, RouteReuseStrategy} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastModule} from 'ng2-toastr';
import { ModalModule } from 'ng2-bootstrap/modal';
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { TooltipModule } from 'ng2-bootstrap/tooltip';
import { TabsModule } from 'ng2-bootstrap/tabs';
import { ProgressbarModule } from 'ng2-bootstrap/progressbar';
import { PopoverModule } from 'ng2-bootstrap/popover';
import { ButtonsModule } from 'ng2-bootstrap/buttons';

import {CapitalizePipe} from "./capitalize.pipe";


import {rootRouterConfig} from './app.routes';

import {NotificationService} from './services/notification.service';
import {AuthService} from './services/auth.service';
import {BungieService} from './services/bungie.service';
import {DestinyCacheService} from './services/destiny-cache.service';
import {GearService} from './services/gear.service';
import {ItemParseService} from './services/item-parse.service';

import {BucketService} from './services/bucket.service';
import {MarkService} from './services/mark.service';


import {AboutComponent} from './about/about.component';
import {GridComponent} from './grid/grid.component';
import {WeaponsComponent} from './weapons/weapons.component';
import {FavsComponent} from './favs/favs.component';
import {ArmorComponent} from './armor/armor.component';
import {EngramsComponent} from './engrams/engrams.component';
import {CharsComponent} from './chars/chars.component';
import {CharComponent} from './chars/char.component';
import {TodoComponent} from './chars/todo.component';
import {BountiesComponent} from './bounties/bounties.component';
import {TogglesComponent} from './filterform/toggles.component';
import { DropdownModule } from './dropdown';

import {StickyReuseStrategy} from './sticky-reuse-strategy';

import {AppComponent} from './app.component';
import {ItemModalComponent} from "./grid/item-modal.component";
import {CsvService} from "./services/csv.service";

@NgModule({
    declarations: [
        CapitalizePipe,
        AppComponent,
        AboutComponent,
        GridComponent,
        WeaponsComponent,
        ArmorComponent,
        EngramsComponent,
        FavsComponent,
        CharsComponent,
        CharComponent,
        TodoComponent,
        BountiesComponent,
        ItemModalComponent,
        TogglesComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ModalModule.forRoot(),
        CollapseModule.forRoot(),
        TooltipModule.forRoot(),
        TabsModule.forRoot(),
        PopoverModule.forRoot(),
        ButtonsModule.forRoot(),
        DropdownModule.forRoot(),
        ProgressbarModule.forRoot(),
        BrowserAnimationsModule,
        ToastModule.forRoot(),
        RouterModule.forRoot(rootRouterConfig, {useHash: true})
    ],
    providers: [
        NotificationService,
        AuthService,
        BungieService,
        DestinyCacheService,
        BucketService,
        GearService,
        ItemParseService,
        MarkService,
        CsvService,
        {provide: RouteReuseStrategy, useClass: StickyReuseStrategy}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
