import {Component, ViewChild, AfterViewInit, ElementRef} from "@angular/core";
import {GearService} from "../services/gear.service";
import {Weapon, DamageType} from "../services/model/item";
import {GridComponent} from "../grid/grid.component";
import {Column} from "../grid/column";
import {ItemModalComponent} from "../grid/item-modal.component";
import {ModalDirective} from "ng2-bootstrap/modal";
import {Subject, Observable} from "rxjs";
import {ItemFilter, Choice, ParsedFilter} from "../filterform/filter";
import {CsvService} from "../services/csv.service";

@Component({
  selector: 'weapons',
  styleUrls: ['./weapons.component.css'],
  templateUrl: './weapons.component.html'
})


export class WeaponsComponent implements AfterViewInit {

  readonly cols: Column[] = [
    new Column('Light', 'light', true),
    new Column('Name', 'name', true),
    new Column('Type', 'type', true, false, true, true),
    new Column("Roll", 'roll', true, false, false, false, false, true),
    new Column("Pvp Roll", 'blessedPvp', true, false, true, true),
    new Column("Pve Roll", 'blessedPve', true, false, true, true),
    new Column("Imp", 'impact', false, false, false, true),
    new Column("Rng", 'range', false, false, false, true),
    new Column("Stab", 'stability', false, false, false, true),
    new Column("ROF", 'rof', false, false, false, true),
    new Column("Rel", 'reload', false, false, false, true),
    new Column("Mag", 'magazine', false, false, false, true),
    new Column("AA", 'aa', false, false, false, true),
    new Column("Inv", 'inv', false, false, false, true),

    //new Column("Opt", 'optics', false, false, false, true),
    new Column("Rec", 'recoil', false, false, false, true),
    new Column("CurXp", 'currentXP', false, false, false, true, true),
    new Column("MaxXp", 'totalXP', false, false, false, true, true),
    new Column('Perks', 'steps', true, true, true, true),
    new Column('Tag', 'mark', true, true)
  ];

  readonly modalFields: string[] = [
    "impact",
    "range",
    "stability",
    "rof",
    "reload",
    "magazine",
    "velocity",
    "chargeRate",
    "speed",
    "defense",
    "energy",
    "efficiency",
    "aa",
    "inv",
    "optics",
    "recoil"
  ];

  @ViewChild('grid') grid: GridComponent;
  @ViewChild('showModal') showModal: ItemModalComponent;


  fixWeaponLocks(){
    this.gearService.fixWeaponLocks();
  }

  downloadCsv(){
    this.csvService.downloadWeaponsCsv();
  }

  //---------FILTER STUFF BELOW

  @ViewChild('searchTxt') input:ElementRef;
  @ViewChild('filterModal') filterModal: ModalDirective;


  private _filterChanged: Subject<void> = new Subject<void>();
  private filterTopic: Observable<void> = this._filterChanged.debounceTime(100);

  readonly DAMAGETYPE = DamageType;
  filter: ItemFilter;


  private buildEmptyFilter(): ItemFilter {
    let filter: ItemFilter = new ItemFilter();
    filter.dmgType = null;
    filter.equipped = null;
    filter.leveled = null;
    filter.maxLight = null;
    filter.minLight = null;
    filter.searchText = null;
    filter.rarity = this.rarityChoices;
    filter.tags = this.markChoices;
    filter.type = this.typeChoices;
    filter.owner =  WeaponsComponent.loadChoices(this.gearService);
    return filter;
  }

  private static selectAllChoices(choices: Choice[]) {
    if (choices == null) return;
    choices.forEach(
      x => x.setValue(true)
    );
  }

  toggleCompare() {
    this.filter.onlyShowCompare=!this.filter.onlyShowCompare;
    this.filterChanged();
  }

  isFiltering(){
    if (this.grid===null) return false;
    return this.grid.count()!=this.grid.totalCount();
  }

  resetFilter() {
    this.filter = this.buildEmptyFilter();
    WeaponsComponent.selectAllChoices(this.rarityChoices);
    WeaponsComponent.selectAllChoices(this.markChoices);
    WeaponsComponent.selectAllChoices(this.typeChoices);
    this.filterChanged();
  }

  constructor(private gearService: GearService, private csvService: CsvService) {
    this.filter = this.buildEmptyFilter();

    this.gearService.refreshedFeed.subscribe(
      x => {
        this.filter.owner =  WeaponsComponent.loadChoices(this.gearService);
        this._filterChanged.next();
      }
    );

    this.filterTopic.subscribe(
      x => {
        this.grid.processList(this.gearService.weapons, this.filter.parse());
      }
    );
  }

  static loadChoices(gearService: GearService){
    let choices: Choice[] = [];
    gearService.chars.forEach(
      (char) => {
        choices.push(new Choice(char.label, char.label));
      }
    );
    choices.push(new Choice("Vault", "Vault"));
    choices.push(new Choice("Vendor", "Vendor"));
    return choices;
  }

  ngAfterViewInit() {
    this.grid.registerModal(this.showModal);
    Observable.fromEvent(
      this.input.nativeElement, 'keyup')
      .map((e: any) => e.target.value)
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe((x: string[]) => {
        this._filterChanged.next();
      });
    //initial load
    this._filterChanged.next();
  }

  filterChanged(): void{
    this._filterChanged.next();
  }

  showFilter() {
    this.filterModal.show();
  }

  readonly markChoices: Choice[] = [
    new Choice("upgrade", "Upgrade"),
    new Choice("keep", "Keep"),
    new Choice("infuse", "Infuse"),
    new Choice("junk", "Junk"),
    new Choice("vendor", "Vendor"),
    new Choice(null, "Not Marked")
  ];

  readonly primaryChoices: Choice[] = [
    new Choice("Auto Rifle", "Auto Rifle"),
    new Choice("Hand Cannon", "Hand Cannon"),
    new Choice("Pulse Rifle", "Pulse"),
    new Choice("Scout Rifle", "Scout"),
  ];


  readonly secondaryChoices: Choice[] = [
    new Choice("Shotgun", "Shotgun"),
    new Choice("Fusion Rifle", "Fusion"),
    new Choice("Sidearm", "Sidearm"),
    new Choice("Sniper Rifle", "Sniper")
  ];

  readonly heavyChoices: Choice[] = [
    new Choice("Sword", "Sword"),
    new Choice("Machine Gun", "Machine Gun"),
    new Choice("Rocket Launcher", "Rocket Launcher")
  ];

  readonly typeChoices: Choice[] = [
    new Choice("primary", "Primary", true, this.primaryChoices),
    new Choice("special", "Special", true, this.secondaryChoices),
    new Choice("heavy", "Heavy", true, this.heavyChoices)
  ];

  readonly rarityChoices: Choice[] = [
    new Choice("Exotic", "Exotic"),
    new Choice("Legendary", "Legendary"),
    new Choice("Rare", "Rare"),
    new Choice("Uncommon", "Uncommon"),
    new Choice("Common", "Common"),
    new Choice("Basic", "Basic")
  ];

}
