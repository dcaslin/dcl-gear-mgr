/**
 * Created by davecaslin on 12/28/16.
 */

export class Column{
  heading: string;
  key: string;
  special: boolean = false;
  noSort: boolean = false;
  hideLabel: boolean = false;
  hiddenXs: boolean = false;
  number: boolean = false;
  xsOnly: boolean = false;

  constructor(heading:string, key:string, special?:boolean, noSort?:boolean, hideLabel?: boolean,
              hiddenXs?: boolean, number?:boolean, xsOnly?:boolean){
    this.heading = heading;
    this.key = key;
    if (special!=null) this.special = special;
    if (noSort!=null) this.noSort = noSort;
    if (hideLabel!=null) this.hideLabel = hideLabel;
    if (hiddenXs!=null) this.hiddenXs = hiddenXs;
    if (number!=null)
      this.number = number;
    if (xsOnly)
      this.xsOnly = xsOnly;
  }
}
