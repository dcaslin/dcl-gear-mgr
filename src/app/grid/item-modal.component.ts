import {Component, ViewChild, Input} from '@angular/core';

import {ModalDirective} from "ng2-bootstrap/modal";
import {Item} from "../services/model/item";
@Component({
  selector: 'item-modal',
  styleUrls: ['./item-modal.component.css'],
  templateUrl: './item-modal.component.html'
})

export class ItemModalComponent {

  @ViewChild('showModal')
  showModal: ModalDirective;

  @Input()
  shown: Item;

  @Input()
  readonly modalFields: string[];

  show(){
    this.showModal.show();
  }
}
