import {Component, ViewChild, AfterViewInit, ElementRef} from "@angular/core";
import {GearService} from "../services/gear.service";
import {DamageType} from "../services/model/item";
import {GridComponent} from "../grid/grid.component";
import {Column} from "../grid/column";
import {ItemModalComponent} from "../grid/item-modal.component";
import {ItemFilter, Choice} from "../filterform/filter";
import {WeaponsComponent} from "../weapons/weapons.component";
import {Observable, Subject} from "rxjs";
import {ModalDirective} from "ng2-bootstrap";
import {CsvService} from "../services/csv.service";
@Component({
  selector: 'armor',
  styleUrls: ['./armor.component.css'],
  templateUrl: './armor.component.html'
})

export class ArmorComponent implements AfterViewInit {

  readonly cols: Column[] = [
    new Column('Light', 'light', true),
    new Column('Name', 'name', true),
    new Column('Type', 'type', true, false, true, true),
    new Column("Class", 'classText', false, false, true, true),
    new Column('Qual', 'qualityPct', true, false, true),
    new Column('Split', 'statSplit', false, false, true, true),
    new Column("% Lvl", 'pctComplete', false, false, false, true),
    new Column('Int', 'int', true, false, false, true),
    new Column('Dis', 'dis', true, false, false, true),
    new Column('Str', 'str', true, false, false, true),

    new Column("CurXp", 'currentXP', false, false, false, true, true),
    new Column("MaxXp", 'totalXP', false, false, false, true, true),

    new Column('Perks', 'steps', true, true, true, true),
    new Column('Mark', 'mark', true, true)
  ];

  readonly modalFields: string[] = [
    "qualityPct",
    "statSplit",
    "int",
    "intPct",
    "dis",
    "disPct",
    "str",
    "strPct"
  ];

  @ViewChild('grid') grid: GridComponent;
  @ViewChild('showModal') showModal: ItemModalComponent;


  fixArmorLocks(){
    this.gearService.fixArmorLocks();
  }

  downloadCsv(){
    this.csvService.downloadArmorCsv();
  }
  //---------FILTER STUFF BELOW

  @ViewChild('searchTxt') input: ElementRef;
  @ViewChild('filterModal') filterModal: ModalDirective;


  private _filterChanged: Subject<void> = new Subject<void>();
  private filterTopic: Observable<void> = this._filterChanged.debounceTime(100);

  readonly DAMAGETYPE = DamageType;
  filter: ItemFilter;

  private buildEmptyFilter(): ItemFilter {
    let filter: ItemFilter = new ItemFilter();
    filter.dmgType = null;
    filter.equipped = null;
    filter.leveled = null;
    filter.maxLight = null;
    filter.minLight = null;
    filter.searchText = null;
    filter.rarity = this.rarityChoices;
    filter.tags = this.markChoices;
    filter.type = this.typeChoices;
    filter.classAllowed = this.classAllowedChoices;
    filter.statSplit = null;
    filter.minQual = null;
    filter.maxQual = null;
    filter.owner = WeaponsComponent.loadChoices(this.gearService);
    return filter;
  }

  private static selectAllChoices(choices: Choice[]) {
    if (choices == null) return;
    choices.forEach(
      x => x.setValue(true)
    );
  }

  resetFilter() {
    this.filter = this.buildEmptyFilter();
    ArmorComponent.selectAllChoices(this.rarityChoices);
    ArmorComponent.selectAllChoices(this.markChoices);
    ArmorComponent.selectAllChoices(this.typeChoices);
    ArmorComponent.selectAllChoices(this.classAllowedChoices);
    this.filterChanged();
  }

  constructor(private gearService: GearService, private csvService: CsvService) {
    this.filter = this.buildEmptyFilter();
    this.gearService.refreshedFeed.subscribe(
      x => {
        this.filter.owner = WeaponsComponent.loadChoices(this.gearService);
        this._filterChanged.next();
      }
    );
    this.filterTopic.subscribe(
      x => {
        this.grid.processList(this.gearService.armor, this.filter.parse());
      }
    );
  }

  toggleCompare() {
    this.filter.onlyShowCompare=!this.filter.onlyShowCompare;
    this.filterChanged();
  }


  isFiltering(){
    if (this.grid===null) return false;
    return this.grid.count()!=this.grid.totalCount();
  }


  ngAfterViewInit() {
    this.grid.registerModal(this.showModal);
    Observable.fromEvent(
      this.input.nativeElement, 'keyup')
      .map((e: any) => e.target.value)
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe((x: string[]) => {
        this._filterChanged.next();
      });
    this._filterChanged.next();
  }

  filterChanged(): void {
    this._filterChanged.next();
  }

  showFilter() {
    this.filterModal.show();
  }


  readonly markChoices: Choice[] = [
    new Choice("upgrade", "Upgrade"),
    new Choice("keep", "Keep"),
    new Choice("infuse", "Infuse"),
    new Choice("junk", "Junk"),
    new Choice("vendor", "Vendor"),
    new Choice(null, "Not Marked")
  ];

  readonly typeChoices: Choice[] = [
    new Choice("Helmet", "Helmet"),
    new Choice("Gauntlets", "Gauntlets"),
    new Choice("Chest Armor", "Chest Armor"),
    new Choice("Leg Armor", "Leg Armor"),
    //class armor is one of 3 types, oddly enough
    new Choice("Hunter Cloak", "Class Armor", true, null, ["Warlock Bond", "Titan Mark"]),
    //so is artifact, oops
    new Choice("Artifact", "Artifact", true, null, ["Titan Artifact", "Hunter Artifact", "Warlock Artifact"]),
    new Choice("Ghost Shell", "Ghost Shell")
  ];

  readonly rarityChoices: Choice[] = [
    new Choice("Exotic", "Exotic"),
    new Choice("Legendary", "Legendary"),
    new Choice("Rare", "Rare"),
    new Choice("Uncommon", "Uncommon"),
    new Choice("Common", "Common"),
    new Choice("Basic", "Basic")
  ];


  readonly classAllowedChoices: Choice[] = [
    new Choice("0", "Titan"),
    new Choice("1", "Hunter"),
    new Choice("2", "Warlock"),
    new Choice("3", "Any"),
  ];

}
