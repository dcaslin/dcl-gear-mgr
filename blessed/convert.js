/**
 * Created by Dave on 1/20/2017.
 */
"use strict";

var fs = require("fs");

function parseWeapon(aEntries) {

}

function parseLines(sCsv) {

  var guns = {};

  var aLines = sCsv.split(/\r?\n/);
  aLines.forEach(
    function (line) {
      var aEntries = line.split(",");
      var name = aEntries.shift();
      var cols = [];
      aEntries.forEach(
        function(slashPerks){
          var aPerk = slashPerks.split("/");
          cols.push(aPerk);
        }
      );
      var addMe ={
        name: name,
        perks: cols
      };
      console.dir(addMe);
      guns[name] = addMe;
    }
  );
  return guns;
}


var sCsv = fs.readFileSync("pve.rollup.csv", "utf-8");
var pveGuns = parseLines(sCsv);
sCsv = fs.readFileSync("pvp.rollup.csv", "utf-8");
var pvpGuns = parseLines(sCsv);

var data = {
  pve: pveGuns,
  pvp: pvpGuns
};

var sData = JSON.stringify(data);
fs.writeFileSync("blessedWeapons.json", sData, "utf-8");
